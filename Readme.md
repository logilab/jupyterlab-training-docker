## BUILD

```sh
make build-python
```

## FEATURES

- jupyterlab-training
- VLC
- lsp
- rql

## Author

<table class="none">
<tr>
<td>
  <img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/3681849/logo.png" width="128">
</td>
<td>
    https://www.logilab.fr
</td>
</tr>
</table>

## Licence
[![Licence: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)

## Acknowledgements

<table class="none">
<tr>
<td>
  <img src="http://opendreamkit.org/public/logos/Flag_of_Europe.svg" width="128">
</td>
<td>
  This package was created as part of the Horizon 2020 European
  Research Infrastructure project
  <a href="https://opendreamkit.org/">OpenDreamKit</a>
  (grant agreement <a href="https://opendreamkit.org/">#676541</a>).
</td>
</tr>
</table>
