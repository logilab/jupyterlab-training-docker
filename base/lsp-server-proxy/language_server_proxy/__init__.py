"""
Return config on servers to start for lsp
"""
import os
import shutil

def setup_lsp():
    def _lsp_command(port):
        full_path = shutil.which('node')
        if not full_path:
            raise FileNotFoundError('Can not find node executable in $PATH')
        serverjs_path = '/opt/conda/share/jupyter/lab/staging/node_modules/jsonrpc-ws-proxy/dist/server.js'
        if not os.path.exists(serverjs_path):
            raise FileNotFoundError('Can not find server.js file')
        serveryml_path = '/home/jovyan/servers.yml'
        if not os.path.exists(serveryml_path):
            raise FileNotFoundError('Can not find servers.yml file')
        return ['node', serverjs_path,  '--port', '3000', '--languageServers', '/home/jovyan/servers.yml']
    return {
        'command': _lsp_command,  #['node', '/home/ogiorgis/Tools/python/envs/python3.5-jupyterlab-1.0.1/share/jupyter/lab/staging/node_modules/jsonrpc-ws-proxy/dist/server.js', '--port', '3000', '--languageServers', '/home/ogiorgis/dev/jupyter/servers.yml'],
        'absolute_url': False,
        'port': 3000,
        'launcher_entry': {
            'enabled': False,
            'icon_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'icons', 'lsp.svg'),
            'title': "lsp",
        }
    }
