import setuptools

setuptools.setup(
    name="jupyter-lsp-proxy",
    version='1.0',
    author="Logilab",
    description="ogiorgis@logilab.fr",
    packages=setuptools.find_packages(),
	keywords=['Jupyter'],
    install_requires=[
        'jupyter-server-proxy'
    ],
    entry_points={
        'jupyter_serverproxy_servers': [
            'lsp = language_server_proxy:setup_lsp',
        ]
    },
    package_data={
        'language_server_proxy': ['icons/*'],
    },
)
