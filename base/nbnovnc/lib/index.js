"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apputils_1 = require("@jupyterlab/apputils");
const coreutils_1 = require("@jupyterlab/coreutils");
const mainmenu_1 = require("@jupyterlab/mainmenu");
const widgets_1 = require("@phosphor/widgets");
var CommandIDs;
(function (CommandIDs) {
    CommandIDs.controlPanel = 'novnc:control-panel';
})(CommandIDs = exports.CommandIDs || (exports.CommandIDs = {}));
const noVncExtension = {
    id: 'jupyterlab-novnc',
    autoStart: true,
    requires: [apputils_1.ICommandPalette, mainmenu_1.IMainMenu],
    activate: activate,
};
function activate(app, palette, mainMenu) {
    const vncBase = coreutils_1.PageConfig.getBaseUrl().replace('http://', '').replace('https://', '');
    const vncSuffix = 'novnc/vnc.html?resize=remote&autoconnect=true';
    const vncURL = coreutils_1.PageConfig.getBaseUrl() + '/novnc/?host=' + vncBase + vncSuffix;
    if (!vncURL) {
        console.log('jupyterlab-vnc: No configuration found.');
        return;
    }
    const category = 'VNC';
    const { commands } = app;
    commands.addCommand(CommandIDs.controlPanel, {
        label: 'Go To VNC',
        caption: 'Open the VNC window in a new browser tab',
        execute: () => {
            window.open(vncURL, '_blank');
        },
    });
    const menu = new widgets_1.Menu({ commands });
    menu.title.label = category;
    [CommandIDs.controlPanel].forEach((command) => {
        palette.addItem({ command, category });
        menu.addItem({ command });
    });
    mainMenu.addMenu(menu, { rank: 100 });
}
exports.default = noVncExtension;
//# sourceMappingURL=index.js.map