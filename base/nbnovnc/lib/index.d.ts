import { JupyterFrontEndPlugin } from '@jupyterlab/application';
export declare namespace CommandIDs {
    const controlPanel: string;
}
declare const noVncExtension: JupyterFrontEndPlugin<void>;
export default noVncExtension;
