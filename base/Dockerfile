FROM jupyter/minimal-notebook:latest

USER root

# debian package
RUN apt update && apt install -y emacs firefox vim curl graphviz gettext \
        supervisor xinit xterm xfce4 x11-xserver-utils xfce4-goodies tigervnc-standalone-server && \
        rm -rf /var/lib/apt/lists/*
RUN apt-get clean -y && apt-get -y autoremove && apt-get -y autoclean

# Jupyterlab extensions
RUN pip install -U pip
RUN pip install -U wheel jupyterlab jupyterhub ipywidgets Pyyaml pyzmq matplotlib ipympl \
    python-language-server[all] pyspark tensorflow keras
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyterlab-python-file --no-build
RUN jupyter labextension install jupyter-matplotlib @krassowski/jupyterlab-lsp @lckr/jupyterlab_variableinspector --no-build
RUN jupyter lab build

# Import matplotlib the first time to build the font cache.
ENV XDG_CACHE_HOME /home/$NB_USER/.cache/
RUN MPLBACKEND=Agg python -c "import matplotlib.pyplot" && fix-permissions /home/$NB_USER

# Jupyterlab-training
WORKDIR /srv/
RUN git clone https://gitlab.com/logilab/jupyterlab-training
WORKDIR /srv/jupyterlab-training
RUN pip install -r requirements.txt
RUN python setup.py install
RUN python setup.py npm_install
RUN jupyter serverextension enable --py jupyterlab_training
WORKDIR /srv/
RUN rm -rf /srv/jupyterlab-training

# jupyterlab-lsp
RUN pip install jupyter-lsp
RUN jupyter serverextension enable --sys-prefix --py jupyter_lsp

# novnc
RUN pip install websockify
RUN wget -qO- https://dl.bintray.com/tigervnc/stable/tigervnc-1.9.0.x86_64.tar.gz | tar xz --strip 1 -C /
RUN mkdir -p /usr/src/app/noVNC
RUN wget -qO- https://github.com/novnc/noVNC/archive/v1.1.0.tar.gz | tar xz --strip 1 -C /usr/src/app/noVNC
# nbnovnc
COPY nbnovnc /srv/nbnovnc
WORKDIR /srv/nbnovnc
RUN pip install .
RUN jupyter serverextension enable --py --sys-prefix nbnovnc
RUN jupyter nbextension install --py --sys-prefix nbnovnc
RUN jupyter nbextension enable --py --sys-prefix nbnovnc
RUN npm install && npm run build && jupyter labextension install .
RUN echo 'c.NBNoVNC.novnc_directory = "/usr/src/app/noVNC"' >> /home/jovyan/.jupyter/jupyter_notebook_config.py
RUN echo 'c.NBNoVNC.vnc_command = "xinit -- /usr/bin/vncserver :99 -SecurityTypes None -fg"' >> /home/jovyan/.jupyter/jupyter_notebook_config.py
RUN echo 'c.NBNoVNC.websockify_command = "websockify --web {novnc_directory} --heartbeat {heartbeat} {port} 0.0.0.0:5999"' >> /home/jovyan/.jupyter/jupyter_notebook_config.py

# clean
RUN jlpm cache clean && npm cache clean --force

# workspace (/home/jovyan)
USER jovyan
WORKDIR /home/jovyan/

# for jupyterlab-lsp
RUN mkdir .lsp_symlink && cd .lsp_symlink && ln -s /home home && ln -s /usr usr
COPY lsp-server-proxy/servers.yml /home/jovyan/servers.yml
COPY --chown=jovyan:users nbnovnc/vnc-startup /home/jovyan/.xinitrc
