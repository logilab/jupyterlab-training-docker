root_dir:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

create-python-workspace:
	if [ -d "./python/training" ]; then \
        echo "workspace already exists"; \
	else \
		echo "python workspace creation";\
		rm -rf /tmp/exercism_worskspace;\
		git clone https://gitlab.com/logilab/exercism_workspace /tmp/exercism_worskspace;\
		cd /tmp/exercism_worskspace; make workspace;\
		cp -r /tmp/exercism_worskspace/_build/jupyter ${root_dir}/python/training;\
    fi

build-base:
	docker build -t jupyterlab-training-base base --rm

build-python: build-base create-python-workspace
	docker build -t jupyterlab-training-python python --rm

build-websem: build-base
	docker build -t jupyterlab-training-websem websem --rm
