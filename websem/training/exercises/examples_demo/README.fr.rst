
Imports
-------

.. code-block:: python
   :class: exercise

   import geopandas as gpd
   import pandas as pda
   import ipywidgets as widgets
   from ipywidgets import interact


Data source: https://opendata.paris.fr


Exemples
--------

Movies : IMDB
~~~~~~~~~~~~~

* Présentation du jeu de données

.. code-block:: python
   :class: exercise

   try:
      movies = pda.read_csv("http://bit.ly/imdbratings")
      print("remote")
   except:
      movies = pda.read_csv("data/imdb.csv")
      print("local")
   movies.head()


* Films les mieux notés

.. code-block:: python
   :class: exercise

   top_movies = movies[movies.star_rating > 8.5].head()
   top_movies


* Films à ne pas voir (selon les avis)

.. code-block:: python
   :class: exercise

   worst_movies = movies.sort_values(by=["star_rating"], ascending=True).head()
   worst_movies


* Les films d'aventure qui durent longtemps

.. code-block:: python
   :class: exercise

   long_adventure_movies = movies[(movies.duration > 200) & (movies.genre == "Adventure")]
   long_adventure_movies


* Films dont le genre est crime ou drame ou action

.. code-block:: python
   :class: exercise

   crime_drama_action_movies = movies[movies.genre.isin(["Crime", "Action", "Drama"])].head()
   crime_drama_action_movies


Films tournés à Paris
~~~~~~~~~~~~~~~~~~~~~

* Présentation du jeu de données

.. code-block:: python
   :class: exercise

   films = gpd.read_file('data/tournagesdefilmsparis2011.geojson')
   films.head()


* Nom du réalisateur et titre du film

.. code-block:: python
   :class: exercise

   real_et_titre = films[["realisateur", "titre"]]
   real_et_titre.drop_duplicates("realisateur").head()


* Types de tournages

.. code-block:: python
   :class: exercise

   type_tournage = films[["type_de_tournage", "organisme_demandeur"]].set_index("type_de_tournage").groupby("type_de_tournage").count()
   type_tournage = type_tournage.sort_values(by="organisme_demandeur", ascending=False).head()
   type_tournage


* Arrondissments attractifs

.. code-block:: python
   :class: exercise

   place_to_film = films[["ardt", "type_de_tournage"]].set_index("ardt").groupby("ardt").count()
   place_to_film = place_to_film.sort_values(by="type_de_tournage", ascending=False).head()
   place_to_film


* Trouver les tournages par arrondissement

.. code-block:: python
   :class: exercise

   plop = films[["ardt", "titre"]].set_index("ardt")
   plop.head()


Réseau cyclable de Paris
~~~~~~~~~~~~~~~~~~~~~~~~

* Présentation du jeu de données

.. code-block:: python
   :class: exercise

   go_for_a_ride = gpd.read_file("data/reseau-cyclable.geojson")
   go_for_a_ride.head()


* Voies les plus "longues"

.. code-block:: python
   :class: exercise

   count = go_for_a_ride[ ["voie", "geometry"] ].groupby("voie").count()
   count.sort_values(by="geometry", ascending=False).head()


* Représentation des voies cycles

.. code-block:: python
   :class: exercise

   lets = go_for_a_ride.plot(figsize=(100, 10), column="voie", linewidth=10)


Arbres de Paris
~~~~~~~~~~~~~~~

* Présentation du jeu de données

Attention le jeu de données est volumineux, ce qui rend le temps de chargement assez long

.. code-block:: python
   :class: exercise

   arbres = gpd.read_file('data/les-arbres.geojson')
   arbres.head()


* Arbres les plus nombreux

.. code-block:: python
   :class: exercise

   top_arbres = arbres[["libellefrancais", "idemplacement"]].set_index("libellefrancais").groupby("libellefrancais").count()
   top_arbres = top_arbres.sort_values(by="idemplacement", ascending=False).head(20)
   top_arbres


* Arbres les plus rares

.. code-block:: python
   :class: exercise

   arbres_rares = arbres[["libellefrancais", "idemplacement"]].set_index("libellefrancais").groupby("libellefrancais").count()
   arbres_rares = arbres_rares.sort_values(by="idemplacement", ascending=True).head(20)
   arbres_rares


* Arbres les plus hauts

.. code-block:: python
   :class: exercise

   arbres_hauts = arbres.set_index("libellefrancais")
   arbres_hauts = arbres_hauts.sort_values(by="hauteurenm", ascending = False).head(20)
   arbres_hauts


* Représentation des arbres

.. code-block:: python
   :class: exercise

   arbres.plot()


* Fond de carte (Paris)

.. code-block:: python
   :class: exercise

   quartiers = gpd.read_file('data/quartier_paris.geojson')
   quartiers.head(1)


.. code-block:: python
   :class: exercise

   quartiers.plot()


* Représentation interactive des espèces d'arbres à Paris

.. code-block:: python
   :class: exercise

   def select_arbre(espèce):
      ax = quartiers.plot(edgecolor='k', linewidth=4, cmap="tab10", alpha=0.5, figsize=(100, 10))
      arbre_to_plot = arbres[ (arbres["libellefrancais"] == espèce) ]
      arbre_to_plot.plot(ax=ax, figsize=(100,10), markersize=100)
      ax.set_axis_off()
      return espèce

   i = interact(select_arbre, espèce = arbres["libellefrancais"].unique())

