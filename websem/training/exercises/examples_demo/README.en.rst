
Imports
-------

.. code-block:: python
   :class: exercise

   import geopandas as gpd
   import pandas as pda
   import ipywidgets as widgets
   from ipywidgets import interact


Data source: https://opendata.paris.fr


Examples
--------

Movies : IMDB
~~~~~~~~~~~~~

* Presentation of the dataset

.. code-block:: python
   :class: exercise

   try:
      movies = pda.read_csv("http://bit.ly/imdbratings")
      print("remote")
   except:
      movies = pda.read_csv("data/imdb.csv")
      print("local")
   movies.head()


* Top rated movies

.. code-block:: python
   :class: exercise

   top_movies = movies[movies.star_rating > 8.5].head()
   top_movies


* Movies not to see (according to reviews)

.. code-block:: python
   :class: exercise

   worst_movies = movies.sort_values(by=["star_rating"], ascending=True).head()
   worst_movies


* Adventure movies that last a long time

.. code-block:: python
   :class: exercise

   long_adventure_movies = movies[(movies.duration > 200) & (movies.genre == "Adventure")]
   long_adventure_movies


* Movies whose genre is crime or drama or action

.. code-block:: python
   :class: exercise

   crime_drama_action_movies = movies[movies.genre.isin(["Crime", "Action", "Drama"])].head()
   crime_drama_action_movies


Films tournés à Paris
~~~~~~~~~~~~~~~~~~~~~

* Presentation of the dataset

.. code-block:: python
   :class: exercise

   films = gpd.read_file('data/tournagesdefilmsparis2011.geojson')
   films.head()


* Director's name and film title

.. code-block:: python
   :class: exercise

   real_et_titre = films[["realisateur", "titre"]]
   real_et_titre.drop_duplicates("realisateur").head()


* Types of filming

.. code-block:: python
   :class: exercise

   type_tournage = films[["type_de_tournage", "organisme_demandeur"]].set_index("type_de_tournage").groupby("type_de_tournage").count()
   type_tournage = type_tournage.sort_values(by="organisme_demandeur", ascending=False).head()
   type_tournage


* Attractive districts

.. code-block:: python
   :class: exercise

   place_to_film = films[["ardt", "type_de_tournage"]].set_index("ardt").groupby("ardt").count()
   place_to_film = place_to_film.sort_values(by="type_de_tournage", ascending=False).head()
   place_to_film


* Find turns by district

.. code-block:: python
   :class: exercise

   plop = films[["ardt", "titre"]].set_index("ardt")
   plop.head()


Paris cycle network
~~~~~~~~~~~~~~~~~~~

* Presentation of the dataset

.. code-block:: python
   :class: exercise

   go_for_a_ride = gpd.read_file("data/reseau-cyclable.geojson")
   go_for_a_ride.head()


* Most "long" lanes

.. code-block:: python
   :class: exercise

   count = go_for_a_ride[ ["voie", "geometry"] ].groupby("voie").count()
   count.sort_values(by="geometry", ascending=False).head()


* Representation of pathways cycles

.. code-block:: python
   :class: exercise

   lets = go_for_a_ride.plot(figsize=(100, 10), column="voie", linewidth=10)


Trees of Paris
~~~~~~~~~~~~~~

* Presentation of the dataset

Be careful the data set is large, which makes the loading time quite long.

.. code-block:: python
   :class: exercise

   arbres = gpd.read_file('data/les-arbres.geojson')
   arbres.head()


* Most numerous trees

.. code-block:: python
   :class: exercise

   top_arbres = arbres[["libellefrancais", "idemplacement"]].set_index("libellefrancais").groupby("libellefrancais").count()
   top_arbres = top_arbres.sort_values(by="idemplacement", ascending=False).head(20)
   top_arbres


* Most rare trees

.. code-block:: python
   :class: exercise

   arbres_rares = arbres[["libellefrancais", "idemplacement"]].set_index("libellefrancais").groupby("libellefrancais").count()
   arbres_rares = arbres_rares.sort_values(by="idemplacement", ascending=True).head(20)
   arbres_rares


* Highest trees

.. code-block:: python
   :class: exercise

   arbres_hauts = arbres.set_index("libellefrancais")
   arbres_hauts = arbres_hauts.sort_values(by="hauteurenm", ascending = False).head(20)
   arbres_hauts


* Representation of trees

.. code-block:: python
   :class: exercise

   arbres.plot()


* Map background (Paris)

.. code-block:: python
   :class: exercise

   quartiers = gpd.read_file('data/quartier_paris.geojson')
   quartiers.head(1)


.. code-block:: python
   :class: exercise

   quartiers.plot()


* Interactive representation of tree species in Paris

.. code-block:: python
   :class: exercise

   def select_arbre(espèce):
      ax = quartiers.plot(edgecolor='k', linewidth=4, cmap="tab10", alpha=0.5, figsize=(100, 10))
      arbre_to_plot = arbres[ (arbres["libellefrancais"] == espèce) ]
      arbre_to_plot.plot(ax=ax, figsize=(100,10), markersize=100)
      ax.set_axis_off()
      return espèce

   i = interact(select_arbre, espèce = arbres["libellefrancais"].unique())

