

Créer une requête sparql qui retourne une Personne (dans le sense de ``foaf``)
qui connaît un parent d'Eric.

.. code-block:: python
   :class: exercise

   sparql_query = """
   Select
   Where {

   }
   """

.. literalinclude:: /exos/modules/Web/sem/sparql_inter/sparql_inter_unittests.py
                    :language: python
                    :class: test

**Solution**

.. literalinclude:: /exos/modules/Web/sem/sparql_inter/sparql_inter_solutions.py
                    :language: python
                    :class: solution
