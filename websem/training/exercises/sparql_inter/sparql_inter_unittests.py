# formation_unittest
"""Check descriptors exercise."""
import unittest
from jupyterlab_training import FormUnitTests
import rdflib


class PersonTest(unittest.TestCase):
    def test_person(self):
        g = rdflib.Graph()

        # formation_solution
        turtle_triple = """@base <http://example.com> .
        @prefix : <http://example.com> .
        @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema> .
        @prefix foaf: <http://xmlns.com/foaf/0.1/> .


        :hasParent rdf:type rdf:Property ;
            rdfs:domain foaf:Person ;
            rdfs:range foaf:Person .


        <#Eric> a foaf:Person ;
                foaf:firstName 'Eric';
                :hasParent <#Marie> .

        <#Marie> a foaf:Person ;
                foaf:firstName 'Marie' .

        <#Fred> a foaf:Person ;
                foaf:firstName 'Fred';
                foaf:knows <#Marie> .

        <#Paul> a foaf:Person ;
                foaf:firstName 'Paul' .
        """

        g.parse(data=turtle_triple, format="turtle")
        try:
            qres = g.query(sparql_query)
        except:
            print("The query is in the wrong format.")
            print("Did you run the cell containing sparql_query?")
            self.fail("The query is in the wrong format.")
        tab_res = [r[0] for r in qres]
        true_res = [rdflib.term.URIRef('http://example.com#Fred')]
        if len(tab_res) != 1:
            print("Your query should return a single result.")
        if true_res != tab_res:
            print("Your query returns:\n", "\n\t".join([r[0] for r in qres]))
            print("instead of ", "\n\t".join(true_res))
            self.fail("Wrong result.")


# formation widget
FormUnitTests(PersonTest);
