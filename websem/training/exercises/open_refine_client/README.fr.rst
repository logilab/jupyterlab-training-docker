
# OpenRefine Client

Initialisation
--------------

Notebook démontrant comment contrôler OpenRefine via un client Python.
Utilisez le fork `dbutlerdb/refine-client-py` de `PaulMakepeace/refine-client-py` pour Python3.

#### Important:
Vous devez lancer openrefine à partir du lanceur jupyterlab avant.


.. code-block:: python
   :class: exercise

   import os
   import pandas as pd
   from open.refine import refine


.. code-block:: python
   :class: exercise

   refine.REFINE_HOST = "localhost"
   refine.REFINE_PORT = "51947"
   server = refine.RefineServer()
   orefine = refine.Refine(server)
   server.server_url()


.. code-block:: python
   :class: exercise

   orefine.list_projects()

**Attention**: si aucun projet n'est présent, créez-en un à partir de l'interface

Ouvrir un projet OpenRefine
---------------------------

Pour ouvrir un projet OpenRefine, nous devons obtenir sa valeur clé du
liste de projets. (Notez que différents projets peuvent avoir le même
nom.)

.. code-block:: python
   :class: exercise

   orefine.list_projects().items()


.. code-block:: python
   :class: exercise

   KEY = list(orefine.list_projects().keys())[0]
   p = orefine.open_project(KEY)


.. code-block:: python
   :class: exercise

   #Inspect the column names
   p.columns


.. code-block:: python
   :class: exercise

   #Rename a column
   col = p.columns[-1]
   p.rename_column(col,'{}2'.format(col))
   p.columns, p.column_order


### Ajouter une colonne

OpenRefine vous permet d'ajouter une colonne dérivée d'une ou de
plusieurs autres colonnes.

```
.add_column(oldColumn, newColumn, expression="value", column_insert_index=None, on_error='set-to-blank'])
```

Le mot-clé `value` indique l'utilisation des valeurs de cellule de
la colonne originale.

.. code-block:: python
   :class: exercise

   p.add_column('name', 'name2', column_insert_index=2)


## Export CSV

L'export semble fonctionner sur l'ensemble de données avec les
filtres actuels. Ceci est distinct de l'ensemble de données complet.


.. code-block:: python
   :class: exercise

   import pandas as pd
   from io import StringIO

   pd.read_csv( StringIO( p.export(export_format='csv') ) )


## Tableau d'affichage avec filtres actuels

Nous pouvons afficher l'état de la table avec le jeu de filtres de
projet en cours.

.. code-block:: python
   :class: exercise


   def show_table(p):
      ''' Display currently selected rows in the table. '''
      cells = [ [col['v'] for col in row['cells']] for row in p.get_rows().rows.rows_response ]
      df = pd.DataFrame( cells )

      #The list of columns seems to include historical items
      #But how do we also guarantee the current one? dicts are inherently unordered?
      cols = ['Unnamed_{}'.format(i) for i in range(len(df.columns))]
      for (k,v) in sorted(p.column_order.items(), key=lambda kv: kv[1]):
           cols[v]=k

      #Set the column names guessed at - is there a better way?
      df.columns = cols

      display ( df ) #columns = [n for n in p.column_order]

   #How do we get the full list of column names?
   show_table(p)


## Manipulation de la table

Comment pouvons-nous manipuler la table de données?

### Noms de colonnes et ordre des colonnes

Nous pouvons rechercher les noms et l’ordre des colonnes actuels de la
manière suivante:

.. code-block:: python
   :class: exercise

   p.column_order


Réorganiser l'ordre des colonnes:

.. code-block:: python
   :class: exercise

   def show_export(p):
      display(pd.read_csv(StringIO(p.export(export_format='csv'))))

   p.reorder_columns(['email','name','state','gender','purchase2','NAME','name2'])
   show_export(p)


.. code-block:: python
   :class: exercise

   p.reorder_columns(['email','name','state','gender','purchase2','NAME','name2'])
   show_export(p)


.. code-block:: python
   :class: exercise

   p.columns


Bien que l'ensemble des colonnes ne soit pas dans la vue exportable de
la table, elles existent toujours:

.. code-block:: python
   :class: exercise

   p.get_rows().rows.rows_response[0]


### Tris

Les données de la table peuvent être triées sur une ou plusieurs
colonnes, chacune croissante ou décroissante.

.. code-block:: python
   :class: exercise

   from open.refine.facet import Sorting
   sorting = Sorting('email')
   sorting.criteria


.. code-block:: python
   :class: exercise

   Sorting(['name', 'gender']).criteria


.. code-block:: python
   :class: exercise

   p.sorting = Sorting('email')
   p.reorder_rows()
   show_table(p)


.. code-block:: python
   :class: exercise

   #Seems we can be more explicit - but are the missing items (e.g. valueType, caseSensitive) important?
   Sorting([{'column':'name','reverse':True}]).criteria


.. code-block:: python
   :class: exercise

   p.sorting = Sorting(['email',{'column':'name','reverse':True}])
   p.reorder_rows()
   show_table(p)


.. code-block:: python
   :class: exercise

   p.get_rows().rows.rows_response[0]


### Filtres non définis

On dirait qu'on peut utiliser `.engine.reset_all()` pour réinitialiser les filtres.

.. code-block:: python
   :class: exercise

   p.engine.reset_all()
   show_table(p)


.. code-block:: python
   :class: exercise

   #The export filter still seems to be set though?
   show_export(p)


### historique du projet

Comment explorons-nous l'historique d'un projet?

.. code-block:: python
   :class: exercise

   def show_history_item(h):
      return h.time, h.id,h.description

   show_history_item(p.history_entry)


## Facettes et clustering

OOpenRefine prend en charge le facettage d’une colonne, y compris le
nombre de facettes, et plusieurs algorithmes de clustering.


.. code-block:: python
   :class: exercise

   from open.refine import facet
   fr=p.compute_facets(facet.TextFacet('name'))
   facets = fr.facets[0]
   for k in sorted(facets.choices,
           key=lambda k: facets.choices[k].count,
           reverse=True):
      print(facets.choices[k].count, k)


.. code-block:: python
   :class: exercise

   facet_value = facet.TextFacet(column='email',
           selection=['danny.baron@example1.com','arthur.duff@example4.com'])
   facet_value.as_dict()


.. code-block:: python
   :class: exercise

   #The export doesn't appear to be affected?
   p.engine.add_facet(facet_value)
   p.compute_facets()

   show_export(p)


.. code-block:: python
   :class: exercise

   #But the table view is?
   show_table(p)


.. code-block:: python
   :class: exercise

   #Reset the facet filter
   facet_value.reset()
   facet_value.as_dict()


.. code-block:: python
   :class: exercise

   show_table(p)


## Clustering

Open Fine prend en charge plusieurs méthodes de clustering:

- clusterer_type: binning; refine_function: fingerprint|metaphone3|cologne-phonetic
- clusterer_type: binning; refine_function: ngram-fingerprint; params: {'ngram-size': INT}
- clusterer_type: knn; refine_function: levenshtein|ppm; params: {'radius': FLOAT,'blocking-ngram-size': INT}



.. code-block:: python
   :class: exercise

   clusters=p.compute_clusters('name',
           clusterer_type='binning',
           refine_function='cologne-phonetic')
   for cluster in clusters:
      print(cluster)


.. code-block:: python
   :class: exercise

   clusters=p.compute_clusters('name',
           clusterer_type='knn',
           refine_function='levenshtein',
           params={'radius':3})
   for cluster in clusters:
      print(cluster)


References:
- https://github.com/PaulMakepeace/refine-client-py/
- https://github.com/psychemedia/jupyterserverproxy-openrefine
- https://github.com/psychemedia/jupyterserverproxy-openrefine/blob/master/notebooks/OpenRefine%20Demos.ipynb
