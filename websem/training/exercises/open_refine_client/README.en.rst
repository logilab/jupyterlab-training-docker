
# OpenRefine Client

Initialisation
--------------

Notebook demonstrating how to control OpenRefine via a Python client.
Use the `dbutlerdb/refine-client-py` fork of `PaulMakepeace/refine-client-py` for Python3 support.

#### Important:
You need to launch openrefine from the jupyterlab launcher before.


.. code-block:: python
   :class: exercise

   import os
   import pandas as pd
   from open.refine import refine


.. code-block:: python
   :class: exercise

   refine.REFINE_HOST = "localhost"
   refine.REFINE_PORT = "51947"
   server = refine.RefineServer()
   orefine = refine.Refine(server)
   server.server_url()


.. code-block:: python
   :class: exercise

   orefine.list_projects()

**Warning**: if no project is present, create one from the openrefine interface.

Opening an OpenRefine Project
-----------------------------

To open an OpenRefine project, we need to get it's key value from the
project list. (Note that different projects may have the same name.)

.. code-block:: python
   :class: exercise

   orefine.list_projects().items()


.. code-block:: python
   :class: exercise

   KEY = list(orefine.list_projects().keys())[0]
   p = orefine.open_project(KEY)


.. code-block:: python
   :class: exercise

   #Inspect the column names
   p.columns


.. code-block:: python
   :class: exercise

   #Rename a column
   col = p.columns[-1]
   p.rename_column(col,'{}2'.format(col))
   p.columns, p.column_order


### Adding A Column

OpenRefine allows you to add a column derived from one or more other columns.

```
.add_column(oldColumn, newColumn, expression="value", column_insert_index=None, on_error='set-to-blank'])
```

The `value` keyword denotes using the cell values from the original
column.

.. code-block:: python
   :class: exercise

   p.add_column('name', 'name2', column_insert_index=2)


## Export CSV

The export appears to work on the dataset with current filters set.
This is distinct from the complere dataset.

.. code-block:: python
   :class: exercise

   import pandas as pd
   from io import StringIO

   pd.read_csv( StringIO( p.export(export_format='csv') ) )


## Display Table With Current Filters

We can display the state of the table with current project filters
set.

.. code-block:: python
   :class: exercise


   def show_table(p):
      ''' Display currently selected rows in the table. '''
      cells = [ [col['v'] for col in row['cells']] for row in p.get_rows().rows.rows_response ]
      df = pd.DataFrame( cells )

      #The list of columns seems to include historical items
      #But how do we also guarantee the current one? dicts are inherently unordered?
      cols = ['Unnamed_{}'.format(i) for i in range(len(df.columns))]
      for (k,v) in sorted(p.column_order.items(), key=lambda kv: kv[1]):
           cols[v]=k

      #Set the column names guessed at - is there a better way?
      df.columns = cols

      display ( df ) #columns = [n for n in p.column_order]

   #How do we get the full list of column names?
   show_table(p)


## Table Manipulation

How can we manipulate the data table?
### Column Names and Column Order

We can look up the current column names and column order as follows:

.. code-block:: python
   :class: exercise

   p.column_order


Rearrange column order:

.. code-block:: python
   :class: exercise

   def show_export(p):
      display(pd.read_csv(StringIO(p.export(export_format='csv'))))

   p.reorder_columns(['email','name','state','gender','purchase2','NAME','name2'])
   show_export(p)


.. code-block:: python
   :class: exercise

   p.reorder_columns(['email','name','state','gender','purchase2','NAME','name2'])
   show_export(p)


.. code-block:: python
   :class: exercise

   p.columns


Although the full set of columns aren't in the exportable view of the table, they do still exist:

.. code-block:: python
   :class: exercise

   p.get_rows().rows.rows_response[0]


### Sorting

Data in the table can be sorted on one or more columns, each ascending or descending.

.. code-block:: python
   :class: exercise

   from open.refine.facet import Sorting
   sorting = Sorting('email')
   sorting.criteria


.. code-block:: python
   :class: exercise

   Sorting(['name', 'gender']).criteria


.. code-block:: python
   :class: exercise

   p.sorting = Sorting('email')
   p.reorder_rows()
   show_table(p)


.. code-block:: python
   :class: exercise

   #Seems we can be more explicit - but are the missing items (e.g. valueType, caseSensitive) important?
   Sorting([{'column':'name','reverse':True}]).criteria


.. code-block:: python
   :class: exercise

   p.sorting = Sorting(['email',{'column':'name','reverse':True}])
   p.reorder_rows()
   show_table(p)


.. code-block:: python
   :class: exercise

   p.get_rows().rows.rows_response[0]


### Unset filters

It looks like we can use `.engine.reset_all()` to reset filters.

.. code-block:: python
   :class: exercise

   p.engine.reset_all()
   show_table(p)


.. code-block:: python
   :class: exercise

   #The export filter still seems to be set though?
   show_export(p)


### Project history

How do we explore a project's history?

.. code-block:: python
   :class: exercise

   def show_history_item(h):
      return h.time, h.id,h.description

   show_history_item(p.history_entry)


## Facets and Clustering

OpenRefine supports facetting on a column, including facet counts, and
several clustering algorithms.

.. code-block:: python
   :class: exercise

   from open.refine import facet
   fr=p.compute_facets(facet.TextFacet('name'))
   facets = fr.facets[0]
   for k in sorted(facets.choices,
           key=lambda k: facets.choices[k].count,
           reverse=True):
      print(facets.choices[k].count, k)


.. code-block:: python
   :class: exercise

   facet_value = facet.TextFacet(column='email',
           selection=['danny.baron@example1.com','arthur.duff@example4.com'])
   facet_value.as_dict()


.. code-block:: python
   :class: exercise

   #The export doesn't appear to be affected?
   p.engine.add_facet(facet_value)
   p.compute_facets()

   show_export(p)


.. code-block:: python
   :class: exercise

   #But the table view is?
   show_table(p)


.. code-block:: python
   :class: exercise

   #Reset the facet filter
   facet_value.reset()
   facet_value.as_dict()


.. code-block:: python
   :class: exercise

   show_table(p)


## Clustering

Open Refine supports several clustering methods:

- clusterer_type: binning; refine_function: fingerprint|metaphone3|cologne-phonetic
- clusterer_type: binning; refine_function: ngram-fingerprint; params: {'ngram-size': INT}
- clusterer_type: knn; refine_function: levenshtein|ppm; params: {'radius': FLOAT,'blocking-ngram-size': INT}



.. code-block:: python
   :class: exercise

   clusters=p.compute_clusters('name',
           clusterer_type='binning',
           refine_function='cologne-phonetic')
   for cluster in clusters:
      print(cluster)


.. code-block:: python
   :class: exercise

   clusters=p.compute_clusters('name',
           clusterer_type='knn',
           refine_function='levenshtein',
           params={'radius':3})
   for cluster in clusters:
      print(cluster)


References:
- https://github.com/PaulMakepeace/refine-client-py/
- https://github.com/psychemedia/jupyterserverproxy-openrefine
- https://github.com/psychemedia/jupyterserverproxy-openrefine/blob/master/notebooks/OpenRefine%20Demos.ipynb
