from random import random

class Barometer():
    def __init__(self, name):
        self.name = name

    def read_Value(self):
        return 1024-20*random()

class LightSensor():
    def __init__(self, name):
        self.name = name

    def read_new_value(self):
        return 450+500*random()

class TemperatureSensor():
    def __init__(self, name):
        self.name = name

    def read(self):
        return 15+10*random()

class Anemometer():
    def __init__(self, name):
        self.name = name

    def value(self):
        return 15*random()
