

## Default values

We assume in the following that the OM2M instance runs on url
OM2M_URL="http://localhost:8082/~/", with a cse id CSE_ID="in-cse/".
The default user name and password will be LOGIN="admin" and PSWD="admin".


.. code-block:: python
   :class: exercise

   # Run this piece of code to load the global variables in the environment
   from om2m_resources.resource import cse, ae, cnt, cin, sub, sgn, smd
   from om2m_client import OM2MClient

   OM2M_URL = "http://localhost:8082/~"
   CSE_ID = "/in-cse/"
   CSE_NAME = "in-name"
   LOGIN="admin"
   PSWD="admin"
   OM2M_BASE = OM2M_URL+CSE_ID


## First manual queries

First, let's run some manual queries against the REST interface.
At any moment, you can visualize the state of the resource tree by visiting http://localhost:8082/webpage (adapted to your local deployment)

### General parameters

Since the platform is RESTful, any query should stand on its own.
In particular, it must include authentication information, with the custom header X-M2M-Origin=LOGIN:PSWD.

### Retrieving the state of a resource

Retrieve operations are implemented in HTTP by a GET. Let's query the root of the architecture:


.. code-block:: python
   :class: exercise

   import requests, json
   auth_headers = {"X-M2M-ORIGIN":LOGIN+":"+PSWD}
   # The other accepted value is application/xml
   common_headers = {"Accept": "application/json"}
   response = requests.get(OM2M_BASE, headers={**auth_headers, **common_headers})
   print(json.dumps(json.loads(response.content), indent=2))


Let's analyze the main points of the returned resource : we have its name ("rn"), its id ("rid"), and its type ("ty"). ty=5 denotes a CSE. Integer values for types are set in the standard, and they will be specified as we go in this tutorial.

### Creating a resource

To create a resource, the representation of what we want to create must be sent to the platform.
Such a representation is either serialized in json or xml, and it must include some attributes as defined in the standard.
The representation is then sent to the platform with a HTTP POST request, towards a resource of the platform which will be its parent.


.. code-block:: python
   :class: exercise

   # Setting up the Application Entity
   header_ae = {"Content-Type":"application/xml;ty=2"}
   # The resource name will be displayed in the resource tree of the Web interface
   name_ae = "JUPYTER_AE"
   body_ae = """
   <om2m:ae xmlns:om2m="http://www.onem2m.org/xml/protocols" rn="{0}">
       <api>app-sensor</api>
       <lbl>Type/sensor Category/temperature Location/home</lbl>
       <rr>false</rr>
   </om2m:ae>
   """.format(name_ae)
   # Sending the Application Entity to OM2M
   response = requests.post(OM2M_BASE, data=body_ae, headers={**auth_headers, **common_headers, **header_ae})
   print(json.dumps(json.loads(response.content), indent=2))


.. code-block:: python
   :class: exercise


   # Setting up the container
   header_cnt = {"Content-Type":"application/xml;ty=3"}
   name_cnt = "JUPYTER_CNT"
   body_cnt = """
   <om2m:cnt xmlns:om2m="http://www.onem2m.org/xml/protocols" rn="{0}">
   </om2m:cnt>
   """.format(name_cnt)
   # Sending the container to OM2M
   response = requests.post(OM2M_BASE+CSE_NAME+"/"+name_ae, data=body_cnt, headers={**auth_headers, **common_headers, **header_cnt})
   print(json.dumps(json.loads(response.content), indent=2))


.. code-block:: python
   :class: exercise

   # Setting up the content instance
   header_cin = {"Content-Type":"application/xml;ty=4"}
   body_cin = """
   <om2m:cin xmlns:om2m="http://www.onem2m.org/xml/protocols">
       <con>3</con>
   </om2m:cin>
   """
   # Sending the container to OM2M
   response = requests.post(OM2M_BASE+CSE_NAME+"/"+name_ae+"/"+name_cnt, data=body_cin, headers={**auth_headers, **common_headers, **header_cin})
   print(json.dumps(json.loads(response.content), indent=2))



# Collecting data in the ADREAM building

## Getting to know the ADREAM building

ADREAM is a smart building collecting data from over 6500 sensors, in particular for the purpose of energy management, at LAAS-CNRS.
The data is available in an open-data: https://syndream.laas.fr:8082/.

## Deploying OM2M for ADREAM

In this session, we will emulate a part of the ADREAM architecture as an OM2M instance.
In this deployment, we will exploit virtual sensors in order to feed two applications: a weather station, and a comfort monitor.
Following is the architecture we want to build.

![Architecture blueprint](adream_om2m.jpg)

## Interfacing OM2M with legacy technologies through an IPE

### Easing the integration of heterogeneous technologies with modularity

To be more realistic, we will assume we collect data from legacy sensors installed at the construction of the building.
Therefore, it will be necessary to have a piece of software to mediate between the standard interface and the ad-hoc sensor API. That module is called an IPE (for Interworking proxy Entity), and it is what allows the integration of heterogeneous technologies into the oneM2M architecture seamlessly from the user point of view.

### The OM2M Client

For simplicity, the "low-level" HTTP queries that we performed so far can be handled by a new component, the OM2MClient.
The following demonstrates a simple usage, before moving on to its integration into the IPE.
The two first parameters for its construction are quite self-explanatory, and the last one is a port on which the client will listen for notifications (we will leave this aside for now).


.. code-block:: python
   :class: exercise

   test_client = OM2MClient(OM2M_URL, CSE_ID, 4567)
   cnt = test_client.create_cnt(CSE_ID+CSE_NAME, "CLIENT_CONTAINER")
   cnt.serialize()


If you check the resource tree, a new container was created. Now, let's look at the IPE code.
The IPE should create two AE, one for each application we consider, and the appropriate Containers.
When accessing the values of the sensors, it should also create the appropriate Content Instances.


.. code-block:: python
   :class: exercise

   from sensors import Barometer, Anemometer, TemperatureSensor, LightSensor

   class Weather_Station():
       def __init__(self):
           self.barometer = Barometer("MyBarometer")
           self.anemometer= Anemometer("MyAnemometer")
           self.temperature= TemperatureSensor("MyThermometer")

   class Room():
       def __init__(self, name):
           self.name = name
           self.temperature= TemperatureSensor(name+"Temperature")
           self.luminosity = LightSensor(name+"Light")

   class ADREAM_IPE():
       def __init__(self):
           self.weather = Weather_Station()
           self.rooms = []
           for room in ["H101, H102, H103"]:
               self.rooms.append(Room(room))
           self.client = OM2MClient(OM2M_URL, CSE_ID, 4568)
           # The parameters are: parent name, resource name, and application ID.
           self.weather_ae = self.client.create_ae(CSE_ID+CSE_NAME, "Weather_Station", "Adream control")
           self.weather_ae_name = CSE_ID+CSE_NAME+"/Weather_Station"
           self.comfort_ae = None # Now, create an AE for the comfort part of the Adream application
           self.comfort_ae_name = None
           self.initialize_weather(self.weather_ae_name)
           self.initialize_comfort(self.comfort_ae_name)

       def initialize_weather(self, ae_name):
           self.client.create_cnt(ae_name, "Anemometer")
           self.client.create_cnt(ae_name+"/Anemometer", "DATA")
           self.client.create_cnt(ae_name, "Thermometer")
           self.client.create_cnt(ae_name+"/Thermometer", "DATA")
           self.client.create_cnt(ae_name, "Barometer")
           self.client.create_cnt(ae_name+"/Barometer", "DATA")

       def initialize_comfort(self, ae_name):
           # Create the Containers for the rooms and their sensors
           None

       def read_values(self):
           # New pressure observation
           pressure = self.weather.barometer.read_Value()
           self.client.create_cin(self.weather_ae_name+"/Barometer/DATA", pressure)
           # New temperature observation
           temperature = self.weather.temperature.read()
           self.client.create_cin(self.weather_ae_name+"/Thermometer/DATA", temperature)
           # New wind speed observation
           wind_speed = self.weather.anemometer.value()
           self.client.create_cin(self.weather_ae_name+"/Anemometer/DATA", wind_speed)
           # Add the reading for the rooms sensors

   #del(ipe)
   ipe = ADREAM_IPE()


Now that the IPE is started, we can ask it to read some values from the sensors.
The read values are then stored in content instances on the OM2M instance.


.. code-block:: python
   :class: exercise

   ipe.read_values()


# Discovery in OM2M

## OM2M discovery mechanism

There are multiple discovery mechanisms in oneM2M. For all, the core concept is the same: a discovery request is sent to a resource,
and the filter contained in this request is applied to all the sub-resources under the target.
Any of these sub-resources matching the filter is returned to the user. In HTTP, the filter is indicated using querystrings.

### Type-based discovery

In this case, the filter is a resource type. Let's discover all the containers under the weather station AE.


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME+"/Weather_Station"
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"ty":3})
   print(json.dumps(json.loads(response.content), indent=2))


### Label-based discovery

Labels are tags attached to resources for the sake of discovery.
A list of labels can be set for any resource using a PUT request as in the followig example:



.. code-block:: python
   :class: exercise

   from om2m_resources.resource import cse, ae, cnt, cin, sub, sgn, smd
   header_cnt = {"Content-Type":"application/json;ty=3"}
   data = cnt()
   # Let us successively add labels to the weather station sensors

   # First, the anemometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Anemometer"
   data.lbl=["type/sensor", "location/outside", "foi/wind"]
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_cnt})

   # Then, the thermometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Thermometer"
   data.lbl=["type/sensor", "location/outside", "foi/temperature"]
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_cnt})

   # And finally, the barometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Barometer"
   data.lbl=["type/sensor", "location/outside", "foi/pressure"]
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_cnt})
   # print(json.dumps(json.loads(response.content), indent=2))


Based on that example, we can add labels to all containers. Then, we can use a list of labels for a filter.
The standard dictates that any resource having any label in common with the filter should be returned. Let's try this out.

.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"lbl":["type/sensor"]})
   print(json.dumps(json.loads(response.content), indent=2))


### Semantic discovery

However, the limits of such a system are quite obvious: the discovery queries lack expresivity.
In order to tackle that, a set of resources have been introduced in order to leverage the expressiveness of ontologies and Semantic Web technologies.

#### Introducing a new resource: the semantic descriptor

A semantic descriptor is a resource that stores a description of its
parent, serialized in RDF/XML and encoded in base64.


.. code-block:: python
   :class: exercise

   import rdflib, base64, json

   # The station's descriptor
   g=rdflib.Graph()
   g.parse("ttl/weather_station.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="STATION_DESCRIPTOR")

   # The anemometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/anemometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="ANEMOMETER_DESCRIPTOR")

   # The barometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/barometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Barometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="BAROMETER_DESCRIPTOR")

   # The thermometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/thermometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Thermometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="THERMOMETER_DESCRIPTOR")


#### Using the semantic descriptor for discovery

In the semantic discovery procedure, the filter is based on SPARQL,
the query language for RDF.
It enables to query for complex patterns in the descriptors.


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   # A simple query: return all the devices
   query = """
   prefix sosa:<http://www.w3.org/ns/sosa/>
   prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>

   SELECT ?device
   WHERE {
       ?device a sosa:Sensor.
   }
   """
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":query})
   print(json.dumps(json.loads(response.content), indent=2))


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   # A more complex one: return all the devices hosted on a platform that observe temperature
   query = """
   prefix sosa:<http://www.w3.org/ns/sosa/>
   prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   prefix ex: <http://example.org/ns#>
   SELECT ?device
   WHERE {
       ?device a sosa:Sensor;
           sosa:isHostedBy ?platform;
           sosa:observes ex:adreamTemperature.
   }
   """
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":query})
   print(json.dumps(json.loads(response.content), indent=2))



Note that so far, all the discovery queries we made were self-contained.
However, an attribute allows to connect semantic descriptors together
accross the platform, in order to create a knowledge graph: the related semantics (rels for short).
Whenever a semantic descriptor points to another with this attribute, the graphs stored in both descriptors are merged as one before being evaluated against the filter query.


.. code-block:: python
   :class: exercise

   from om2m_resources.resource import smd

   station_desc = CSE_ID+CSE_NAME+"/Weather_Station/STATION_DESCRIPTOR"
   anemometer_desc = CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/ANEMOMETER_DESCRIPTOR"
   thermometer_desc = CSE_ID+CSE_NAME+"/Weather_Station/Thermometer/THERMOMETER_DESCRIPTOR"
   barometer_desc = CSE_ID+CSE_NAME+"/Weather_Station/Barometer/BAROMETER_DESCRIPTOR"

   # First, delete the previous descriptors
   target = OM2M_URL+anemometer_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})
   target = OM2M_URL+thermometer_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})
   target = OM2M_URL+barometer_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})
   target = OM2M_URL+station_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})

   # Then, rebuild them with the related semantics attribute
   header_smd = {"Content-Type":"application/json;ty=24"}
   data = smd()

   # Let us successively add connections between descriptors

   # The station's descriptor
   g=rdflib.Graph()
   g.parse("ttl/weather_station.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="STATION_DESCRIPTOR",
                                     rels=[anemometer_desc, thermometer_desc, barometer_desc])

   # The anemometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/anemometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="ANEMOMETER_DESCRIPTOR",
                                     rels=station_desc)

   # The barometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/barometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Barometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="BAROMETER_DESCRIPTOR",
                                    rels=station_desc)

   # The thermometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/thermometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Thermometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="THERMOMETER_DESCRIPTOR",
                                     rels=station_desc)


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   # A more complex one: return all the platforms hosting at least one temperature and one wind speed sensor
   query = """
   prefix sosa:<http://www.w3.org/ns/sosa/>
   prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   prefix ex: <http://example.org/ns#>
   SELECT ?platform
   WHERE {
       ?platform ex:located ex:outside;
           sosa:hosts ?sensor.
       ?device a sosa:Sensor;
           sosa:isHostedBy ?platform;
           sosa:observes ex:adreamTemperature.
       ?sensor sosa:observes  ex:adreamWindSpeed.
   }
   """
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":query})
   print(json.dumps(json.loads(response.content), indent=2))
