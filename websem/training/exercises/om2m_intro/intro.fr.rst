# Essayer l'interface OM2M

## Valeurs pae défaut

Nous supposons par la suite que l’instance OM2M s’exécute sur l’URL OM2M_URL="http://localhost:8082/~/", avec un identifiant cse
CSE_ID="in-cse/". Le nom d'utilisateur et le mot de passe par défaut seront LOGIN="admin" et PSWD="admin". Tout d'abord, vous
pouvez visiter la page Web de la plate-forme pour visualiser son arborescence de ressources à l'adresse http://localhost:8082/webpage


.. code-block:: python
   :class: exercise

   # Run this piece of code to load the global variables in the environment
   from om2m_resources.resource import cse, ae, cnt, cin, sub, sgn, smd
   from om2m_client import OM2MClient

   OM2M_URL = "http://localhost:8082/~"
   CSE_ID = "/in-cse/"
   CSE_NAME = "in-name"
   LOGIN="admin"
   PSWD="admin"
   OM2M_BASE = OM2M_URL+CSE_ID


## Premières requêtes manuelles

Commençons par lancer des requêtes manuelles sur l'interface REST.
A tout moment, vous pouvez visualiser l'état de l'arborescence des
ressources en visitant http://localhost:8082/webpage  (adapté à votre déploiement local).


### Paramètres généraux

Comme la plate-forme est RESTful, toute requête doit être autonome.
En particulier, il doit inclure les informations d'authentification,
avec l'en-tête personnalisé X-M2M-Origin = LOGIN: PSWD.

### Récupération de l'état d'une ressource

Les opérations de récupération sont implémentées dans HTTP par un GET.
Interrogeons la racine de l'architecture:


.. code-block:: python
   :class: exercise

   import requests, json
   auth_headers = {"X-M2M-ORIGIN":LOGIN+":"+PSWD}
   # The other accepted value is application/xml
   common_headers = {"Accept": "application/json"}
   response = requests.get(OM2M_BASE, headers={**auth_headers, **common_headers})
   print(json.dumps(json.loads(response.content), indent=2))


Analysons les points principaux de la ressource renvoyée: nous avons
son nom ("rn"), son id ("rid") et son type ("ty"). ty = 5 indique un CSE.
Les valeurs entières des types sont définies dans la norme et
seront spécifiées au fur et à mesure de ce didacticiel.

### Créer une ressource

Pour créer une ressource, la représentation de ce que nous voulons créer doit être envoyée à la plateforme.
Une telle représentation est sérialisée en json ou xml et doit inclure certains attributs tels que
définis dans la norme.
La représentation est ensuite envoyée à la plate-forme avec une requête HTTP POST, vers une ressource de la
plate-forme qui en sera le parent.

.. code-block:: python
   :class: exercise

   # Setting up the Application Entity
   header_ae = {"Content-Type":"application/xml;ty=2"}
   # The resource name will be displayed in the resource tree of the Web interface
   name_ae = "JUPYTER_AE"
   body_ae = """
   <om2m:ae xmlns:om2m="http://www.onem2m.org/xml/protocols" rn="{0}">
       <api>app-sensor</api>
       <lbl>Type/sensor Category/temperature Location/home</lbl>
       <rr>false</rr>
   </om2m:ae>
   """.format(name_ae)
   # Sending the Application Entity to OM2M
   response = requests.post(OM2M_BASE, data=body_ae, headers={**auth_headers, **common_headers, **header_ae})
   print(json.dumps(json.loads(response.content), indent=2))


.. code-block:: python
   :class: exercise


   # Setting up the container
   header_cnt = {"Content-Type":"application/xml;ty=3"}
   name_cnt = "JUPYTER_CNT"
   body_cnt = """
   <om2m:cnt xmlns:om2m="http://www.onem2m.org/xml/protocols" rn="{0}">
   </om2m:cnt>
   """.format(name_cnt)
   # Sending the container to OM2M
   response = requests.post(OM2M_BASE+CSE_NAME+"/"+name_ae, data=body_cnt, headers={**auth_headers, **common_headers, **header_cnt})
   print(json.dumps(json.loads(response.content), indent=2))


.. code-block:: python
   :class: exercise

   # Setting up the content instance
   header_cin = {"Content-Type":"application/xml;ty=4"}
   body_cin = """
   <om2m:cin xmlns:om2m="http://www.onem2m.org/xml/protocols">
       <con>3</con>
   </om2m:cin>
   """
   # Sending the container to OM2M
   response = requests.post(OM2M_BASE+CSE_NAME+"/"+name_ae+"/"+name_cnt, data=body_cin, headers={**auth_headers, **common_headers, **header_cin})
   print(json.dumps(json.loads(response.content), indent=2))



# Collecte de données dans le bâtiment ADREAM

## Découvrir le bâtiment ADREAM

ADREAM est un bâtiment intelligent qui collecte les données de plus de
6500 capteurs, en particulier pour la gestion de l'énergie, au LAAS-CNRS.
Les données sont disponibles sous forme de données ouvertes: https://syndream.laas.fr:8082/


## Déployer OM2M pour ADREAM

Dans cette session, nous allons émuler une partie de l’architecture ADREAM en tant qu’instance OM2M.
Dans ce déploiement, nous allons exploiter des capteurs virtuels pour alimenter deux applications: une
station météo et un moniteur de confort.
Voici l’architecture que nous voulons construire.

![Architecture blueprint](adream_om2m.jpg)

## Interfacer OM2M avec les technologies traditionnelles via un IPE

### Faciliter l'intégration de technologies hétérogènes grâce à la
    modularité

Pour être plus réaliste, nous supposerons que nous collectons des
données à partir de capteurs hérités installés lors de la construction
du bâtiment.
Par conséquent, il sera nécessaire de disposer d’un
logiciel servant de médiateur entre l’interface standard et l’API du
capteur ad-hoc. Ce module est appelé un IPE (pour Interworking Proxy
Entity), et c’est ce qui permet l’intégration transparente de
technologies hétérogènes dans l’architecture oneM2M du point de vue de
l’utilisateur.

### Le client OM2M

Par souci de simplicité, les requêtes HTTP "de bas niveau" que nous
avons effectuées jusqu’à présent peuvent être gérées par un nouveau
composant, le OM2MClient. Ce qui suit illustre une utilisation simple,
avant de passer à son intégration dans l'IPE. Les deux premiers
paramètres pour sa construction sont assez explicites, et le dernier
est un port sur lequel le client écoutera les notifications (nous
laisserons cela de côté pour l'instant).


.. code-block:: python
   :class: exercise

   test_client = OM2MClient(OM2M_URL, CSE_ID, 4567)
   cnt = test_client.create_cnt(CSE_ID+CSE_NAME, "CLIENT_CONTAINER")
   cnt.serialize()


Si vous vérifiez l'arborescence des ressources, un nouveau conteneur a
été créé. Maintenant, regardons le code IPE. L'IPE doit créer deux AE,
un pour chaque application considérée et les conteneurs appropriés.
Lors de l'accès aux valeurs des capteurs, il convient également de
créer les instances de contenu appropriées.


.. code-block:: python
   :class: exercise

   from sensors import Barometer, Anemometer, TemperatureSensor, LightSensor

   class Weather_Station():
       def __init__(self):
           self.barometer = Barometer("MyBarometer")
           self.anemometer= Anemometer("MyAnemometer")
           self.temperature= TemperatureSensor("MyThermometer")

   class Room():
       def __init__(self, name):
           self.name = name
           self.temperature= TemperatureSensor(name+"Temperature")
           self.luminosity = LightSensor(name+"Light")

   class ADREAM_IPE():
       def __init__(self):
           self.weather = Weather_Station()
           self.rooms = []
           for room in ["H101, H102, H103"]:
               self.rooms.append(Room(room))
           self.client = OM2MClient(OM2M_URL, CSE_ID, 4568)
           # The parameters are: parent name, resource name, and application ID.
           self.weather_ae = self.client.create_ae(CSE_ID+CSE_NAME, "Weather_Station", "Adream control")
           self.weather_ae_name = CSE_ID+CSE_NAME+"/Weather_Station"
           self.comfort_ae = None # Now, create an AE for the comfort part of the Adream application
           self.comfort_ae_name = None
           self.initialize_weather(self.weather_ae_name)
           self.initialize_comfort(self.comfort_ae_name)

       def initialize_weather(self, ae_name):
           self.client.create_cnt(ae_name, "Anemometer")
           self.client.create_cnt(ae_name+"/Anemometer", "DATA")
           self.client.create_cnt(ae_name, "Thermometer")
           self.client.create_cnt(ae_name+"/Thermometer", "DATA")
           self.client.create_cnt(ae_name, "Barometer")
           self.client.create_cnt(ae_name+"/Barometer", "DATA")

       def initialize_comfort(self, ae_name):
           # Create the Containers for the rooms and their sensors
           None

       def read_values(self):
           # New pressure observation
           pressure = self.weather.barometer.read_Value()
           self.client.create_cin(self.weather_ae_name+"/Barometer/DATA", pressure)
           # New temperature observation
           temperature = self.weather.temperature.read()
           self.client.create_cin(self.weather_ae_name+"/Thermometer/DATA", temperature)
           # New wind speed observation
           wind_speed = self.weather.anemometer.value()
           self.client.create_cin(self.weather_ae_name+"/Anemometer/DATA", wind_speed)
           # Add the reading for the rooms sensors

   #del(ipe)
   ipe = ADREAM_IPE()


Maintenant que l'IPE est démarré, nous pouvons lui demander de lire
certaines valeurs des capteurs. Les valeurs lues sont ensuite stockées
dans des instances de contenu sur l'instance OM2M.


.. code-block:: python
   :class: exercise

   ipe.read_values()


# Découverte dans OM2M

## Mécanisme de découverte de OM2M

Il existe plusieurs mécanismes de découverte dans oneM2M. Pour tous,
le concept de base est le même: une demande de découverte est envoyée
à une ressource, et le filtre contenu dans cette requête est appliqué
à toutes les sous-ressources de la cible. Chacune de ces
sous-ressources correspondant au filtre est renvoyée à l'utilisateur.
En HTTP, le filtre est indiqué en utilisant des chaînes de requête.

### Découverte basée sur un type

Dans ce cas, le filtre est un type de ressource. Découvrons tous les
conteneurs sous la station météo AE.


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME+"/Weather_Station"
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"ty":3})
   print(json.dumps(json.loads(response.content), indent=2))


### écouverte basée sur les étiquettes

Les étiquettes sont des étiquettes attachées à des ressources à des
fins de découverte. Une liste d'étiquettes peut être définie pour
n'importe quelle ressource en utilisant une requête PUT comme dans
l'exemple suivant:


.. code-block:: python
   :class: exercise

   from om2m_resources.resource import cse, ae, cnt, cin, sub, sgn, smd
   header_cnt = {"Content-Type":"application/json;ty=3"}
   data = cnt()
   # Let us successively add labels to the weather station sensors

   # First, the anemometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Anemometer"
   data.lbl=["type/sensor", "location/outside", "foi/wind"]
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_cnt})

   # Then, the thermometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Thermometer"
   data.lbl=["type/sensor", "location/outside", "foi/temperature"]
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_cnt})

   # And finally, the barometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Barometer"
   data.lbl=["type/sensor", "location/outside", "foi/pressure"]
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_cnt})
   # print(json.dumps(json.loads(response.content), indent=2))


Sur la base de cet exemple, nous pouvons ajouter des étiquettes à tous
les conteneurs. Ensuite, nous pouvons utiliser une liste d'étiquettes
pour un filtre. La norme stipule que toute ressource ayant une
étiquette en commun avec le filtre doit être renvoyée. Essayons ceci:

.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"lbl":["type/sensor"]})
   print(json.dumps(json.loads(response.content), indent=2))


### Découverte sémantique

Cependant, les limites d'un tel système sont assez évidentes: les
requêtes de découverte manquent d'expressivité. Afin de résoudre ce
problème, un ensemble de ressources a été introduit afin de tirer
parti de l’expressivité des ontologies et des technologies du Web
sémantique.

#### Introduction d'une nouvelle ressource: le descripteur sémantique

Un descripteur sémantique est une ressource qui stocke une description
de son parent, sérialisé en RDF / XML et codé en base64.


.. code-block:: python
   :class: exercise

   import rdflib, base64, json

   # The station's descriptor
   g=rdflib.Graph()
   g.parse("ttl/weather_station.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="STATION_DESCRIPTOR")

   # The anemometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/anemometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="ANEMOMETER_DESCRIPTOR")

   # The barometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/barometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Barometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="BAROMETER_DESCRIPTOR")

   # The thermometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/thermometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Thermometer",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="THERMOMETER_DESCRIPTOR")


#### Utilisation du descripteur sémantique pour la découverte

Dans la procédure de découverte sémantique, le filtre est basé sur
SPARQL, le langage de requête pour RDF. Il permet de rechercher des
modèles complexes dans les descripteurs.


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   # A simple query: return all the devices
   query = """
   prefix sosa:<http://www.w3.org/ns/sosa/>
   prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>

   SELECT ?device
   WHERE {
       ?device a sosa:Sensor.
   }
   """
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":query})
   print(json.dumps(json.loads(response.content), indent=2))


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   # A more complex one: return all the devices hosted on a platform that observe temperature
   query = """
   prefix sosa:<http://www.w3.org/ns/sosa/>
   prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   prefix ex: <http://example.org/ns#>
   SELECT ?device
   WHERE {
       ?device a sosa:Sensor;
           sosa:isHostedBy ?platform;
           sosa:observes ex:adreamTemperature.
   }
   """
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":query})
   print(json.dumps(json.loads(response.content), indent=2))


Notez que jusqu'à présent, toutes les requêtes de découverte que nous
avons effectuées étaient autonomes. Cependant, un attribut permet de
relier des descripteurs sémantiques sur toute la plate-forme, afin de
créer un graphe de connaissances: la sémantique associée (rels en
abrégé). Chaque fois qu'un descripteur sémantique pointe vers un autre
avec cet attribut, les graphes stockés dans les deux descripteurs sont
fusionnés avant d'être évalués par rapport à la requête de filtrage.


.. code-block:: python
   :class: exercise

   from om2m_resources.resource import smd

   station_desc = CSE_ID+CSE_NAME+"/Weather_Station/STATION_DESCRIPTOR"
   anemometer_desc = CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/ANEMOMETER_DESCRIPTOR"
   thermometer_desc = CSE_ID+CSE_NAME+"/Weather_Station/Thermometer/THERMOMETER_DESCRIPTOR"
   barometer_desc = CSE_ID+CSE_NAME+"/Weather_Station/Barometer/BAROMETER_DESCRIPTOR"

   # First, delete the previous descriptors
   target = OM2M_URL+anemometer_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})
   target = OM2M_URL+thermometer_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})
   target = OM2M_URL+barometer_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})
   target = OM2M_URL+station_desc
   response = requests.delete(target, headers={**auth_headers, **common_headers})

   # Then, rebuild them with the related semantics attribute
   header_smd = {"Content-Type":"application/json;ty=24"}
   data = smd()

   # Let us successively add connections between descriptors

   # The station's descriptor
   g=rdflib.Graph()
   g.parse("ttl/weather_station.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station", 
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="STATION_DESCRIPTOR", 
                                     rels=[anemometer_desc, thermometer_desc, barometer_desc])

   # The anemometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/anemometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer", 
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="ANEMOMETER_DESCRIPTOR", 
                                     rels=station_desc)

   # The barometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/barometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Barometer", 
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="BAROMETER_DESCRIPTOR", 
                                    rels=station_desc)

   # The thermometer's descriptor
   g=rdflib.Graph()
   g.parse("ttl/thermometer.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Thermometer", 
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     name="THERMOMETER_DESCRIPTOR", 
                                     rels=station_desc)


.. code-block:: python
   :class: exercise

   target = OM2M_BASE+CSE_NAME
   # A more complex one: return all the platforms hosting at least one temperature and one wind speed sensor 
   query = """
   prefix sosa:<http://www.w3.org/ns/sosa/>
   prefix rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
   prefix ex: <http://example.org/ns#>
   SELECT ?platform
   WHERE {
       ?platform ex:located ex:outside;
           sosa:hosts ?sensor.
       ?device a sosa:Sensor;
           sosa:isHostedBy ?platform;
           sosa:observes ex:adreamTemperature.
       ?sensor sosa:observes  ex:adreamWindSpeed.
   }
   """
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":query})
   print(json.dumps(json.loads(response.content), indent=2))
