import json
import requests
from . import constants

class request_primitive():
    def __init__(self, originator, to, content, operation):
        self.originator = originator
        self.to = to
        self.content = content
        self.operation = operation

    def execute(self):
        headers = {"X-M2M-ORIGIN":self.originator, "Content-Type": "application/json", "Accept": "application/json"}
        if self.operation == constants.RETRIEVE:
            return requests.get(self.to, headers=headers)
        elif self.operation == constants.CREATE:
            headers["Content-Type"] = headers["Content-Type"]+";ty="+str(self.content.resource_type)
            return requests.post(self.to, headers=headers, data=self.content.serialize())
