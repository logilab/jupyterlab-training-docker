import json

class om2m_resource():
    def __init__(self, json_obj=None, json_root=None):
        self.json_root = json_root
        self.resource_type = -1
        if json_obj != None:
            self.json = json.loads(json_obj)
            for attr in self.json[json_root]:
                super().__setattr__(attr, self.json[json_root][attr])

    def serialize(self):
        return json.dumps({self.json_root:self.__dict__})

# Commin service entity
class cse(om2m_resource):
    def __init__(self, json_obj=None):
        self.json_root = "m2m:cb"
        super().__init__(json_obj, self.json_root)
        self.resource_type = 5

# Application entity
class ae(om2m_resource):
    def __init__(self, json_obj=None):
        self.json_root = "m2m:ae"
        super().__init__(json_obj, self.json_root)
        self.resource_type = 2

# Container
class cnt(om2m_resource):
    def __init__(self, json_obj=None):
        self.json_root = "m2m:cnt"
        super().__init__(json_obj, self.json_root)
        self.resource_type = 3

# Content instance
class cin(om2m_resource):
    def __init__(self, json_obj=None,):
        self.json_root = "m2m:cin"
        super().__init__(json_obj, self.json_root)
        self.resource_type = 4

# Subscription
class sub(om2m_resource):
    def __init__(self, json_obj=None,):
        self.json_root = "m2m:sub"
        super().__init__(json_obj, self.json_root)
        self.resource_type = 23

# Notification
class sgn(om2m_resource):
    def __init__(self, json_obj=None,):
        self.json_root = "m2m:sgn"
        super().__init__(json_obj, self.json_root)

# Semantic descriptor
class smd(om2m_resource):
    def __init__(self, json_obj=None,):
        self.json_root = "m2m:smd"
        super().__init__(json_obj, self.json_root)
        self.resource_type = 24
