**Introduction à Cubicweb**
===========================

Cubicweb installation
=====================

Ouvrir un terminal dans jupyterlab (**File->New Launcher->terminal**) et installer cubicweb :

```
$> pip install cubicweb['pyramid']
```

<sub>**! ATTENTION** Si vous éxécutez ce tutoriel sur **votre poste**, travaillez dans un environnement virtuel (`python3 -m venv cubicenv` && `. cubicenv/bin/activate`)</sub>

cubicweb-ctl
============

Dans le terminal, tester les commandes suivantes.

Découvertes des commandes
~~~~~~~~~~~~~~~~~~~~~~~~~

* La liste des commandes s’obtient avec l’option --help :

```
$> cubicweb-ctl --help
```

* L’aide sur l’utilisation d’une commande s’obtient également avec l’option --help :

```
$> cubicweb-ctl <nom-de-la-commande> --help
```

* La liste des cubes et instances s’obtient avec la commande list :

```
$> cubicweb-ctl list
```

Création de cube ou d’instance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* La commande newcube permet de créer un nouveau cube :

```
$> cubicweb-ctl newcube <nom-cube>
```

Une description est demandée, et l’infrastructure de base est créée. L’option --directory permet de préciser le répertoire de création du cube.

* La commande create permet de créer une nouvelle instance :

```
$> cubicweb-ctl create <nom-cube> <nom-instance>
```

Si les choix par défaut sont tous conservés, il faut seulement préciser le login/mdp de l’instance (à choisir) et le login/mdp de d’accès à la base de données.

* La commande delete permet de supprimer une instance :

```
$> cubicweb-ctl delete <nom-instance>
```

Gestion de la base de données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Les commandes db-create et db-init sont responsables de la création de la base de données système (création de la base, des tables et index nécessaires) et de son initialisation (écriture du
modèle de données en base).
Ces commandes sont par défaut enchaînées automatiquement par la commande create, mais elle
permettent de reprendre à un point plus précis du processus.

* La commande db-dump permet de sauvegarder la base de données :

```
$> cubicweb-ctl db-dump <nom-instance>
```

* La commande db-restore permet de recharger la base de données :

```
$> cubicweb-ctl db-restore <nom-instance> <nom-du-dump>
```

Gestion d’une instance
~~~~~~~~~~~~~~~~~~~~~~

* L’instance peut être démarrée avec la commande start :

```
$> cubicweb-ctl start -D <nom-instance>
```

L’option -D permet de lancer le mode “debug”. L’application ne passe pas en mode “serveur” et reste connectée au terminal.

* L’instance peut être arrêtée avec la commande stop :

```
$> cubicweb-ctl stop <nom-instance>
```

* L’instance peut être redémarrée avec la commande restart :

```
$> cubicweb-ctl restart <nom-instance>
```

Une fois démarrée, l’instance devient ensuite accessible via l’url http://localhost:8080 (l’url est configu- rable dans le all-in-one.conf, option base-url).

Shell Python sur une instance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CubicWeb propose un shell Python augmenté de différentes fonctions permettant une interaction facile avec les données de l’application :

```
$> cubicweb-ctl shell <nom-instance>
```

Depuis ce shell, vous pourrez par exemple accéder aux fonctions suivantes :

    * ajout (add_cube ) et suppression (remove_cube ) d’un cube,
    * gestion du schéma (schema ) :
    * ajout (add_entity_type ) et suppression (drop_entity_type ) d’un type d’entité,
    * ajout (add_attribute ) et suppression (drop_attribute ) d’un attribut,
    * ajout (add_relation_definition ) et suppression (drop_relation_definition ) d’une définition de relation.
    * session (session), connexion sur la base de données via la DB-API (cnx, rollback , commit ),
    * création (create_entity ) et recherche (find_one_entity , session.entity_from_eid ) d’une entité,
    * exécution de requête (sql ou rql ).

