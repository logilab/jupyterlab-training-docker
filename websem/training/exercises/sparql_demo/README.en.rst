

I. Preparation
--------------

The aim of this notebook is to showcase some of the possibilities
offered by the Semantic Web and to offer a playground for further
explorations on Bnf, Wikidata, DBPedia and the Noble Prize websites.  
To get some visual representation of how is composed the **Linked Open
Data**, you can have a look at this interactive schema : [The Linked
Open Data Cloud](https://lod-cloud.net/)  
A good place to start: [Query Wikidata.org](https://query.wikidata.org/) (tips: info on mouse hover)


Useful libraries import
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   from gastrodon import RemoteEndpoint, inline
   import ipywidgets as widgets
   from IPython.core.display import display, HTML
   from SPARQLWrapper import SPARQLWrapper, JSON


Prefixes: shortcuts for namespaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   prefixes = inline("""
      @prefix ark: <http://ark.bnf.fr/ark:/12148/> .
      @prefix bibo: <http://purl.org/ontology/bibo/> .
      @prefix bio: <http://vocab.org/bio/0.1/> .
      @prefix bnf-onto: <http://data.bnf.fr/ontology/bnf-onto/> .
      @prefix bnfroles: <http://data.bnf.fr/vocabulary/roles/> .
      @prefix dbpedia: <http://dbpedia.org/> .
      @prefix dbpediaowl: <http://dbpedia.org/ontology/> .
      @prefix dbprop: <http://dbpedia.org/property/> .
      @prefix dcdoc: <http://dublincore.org/documents/> .
      @prefix dcmitype: <http://purl.org/dc/dcmitype/> .
      @prefix dcterms: <http://purl.org/dc/terms/> .
      @prefix foaf: <http://xmlns.com/foaf/0.1/> .
      @prefix frbr: <http://rdvocab.info/uri/schema/FRBRentitiesRDA/> .
      @prefix geo: <http://www.w3.org/2003/01/geo/wgs84_pos#> .
      @prefix gn: <http://www.geonames.org/ontology/ontology_v3.1.rdf/> .
      @prefix ign: <http://data.ign.fr/ontology/topo.owl/> .
      @prefix insee: <http://rdf.insee.fr/geo/> .
      @prefix isni: <http://isni.org/ontology#> .
      @prefix marcrel: <http://id.loc.gov/vocabulary/relators/> .
      @prefix mo: <http://purl.org/ontology/mo/> .
      @prefix og: <http://ogp.me/ns#> .
      @prefix ore: <http://www.openarchives.org/ore/terms/> .
      @prefix owl: <http://www.w3.org/2002/07/owl#> .
      @prefix prov: <http://www.w3.org/ns/prov#> .
      @prefix rdagroup1elements: <http://rdvocab.info/Elements/> .
      @prefix rdagroup2elements: <http://rdvocab.info/ElementsGr2/> .
      @prefix rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/> .
      @prefix rdarole: <http://rdvocab.info/roles/> .
      @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
      @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
      @prefix schema: <http://schema.org/> .
      @prefix skos: <http://www.w3.org/2004/02/skos/core#> .
      @prefix time: <http://www.w3.org/TR/owl-time/> .
      @prefix xfoaf: <http://www.foafrealm.org/xfoaf/0.1/> .
      @prefix xml: <http://www.w3.org/XML/1998/namespace> .
      @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
      @prefix nobel: <http://data.nobelprize.org/terms/> .
   """).graph


Definition of the SPARQL connection points
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   BNF_ENDPOINT = RemoteEndpoint(
      "http://data.bnf.fr/sparql",
      default_graph = "http://data.bnf.fr",
      prefixes = prefixes,
      base_uri = "http://data.bnf.fr/"
   )
   WIKIDATA_ENDPOINT = RemoteEndpoint(
      "https://query.wikidata.org/sparql",
      default_graph = "https://query.wikidata.org",
      prefixes = prefixes,
      base_uri = "https://query.wikidata.org/wiki"
   )
   DBPEDIA_ENDPOINT = RemoteEndpoint(
       "http://dbpedia.org/sparql",
       default_graph = "http://dbpedia.org",
       prefixes = prefixes,
       base_uri = "http://dbpedia.org/"
   )
   NOBEL_ENDPOINT = RemoteEndpoint(
       "http://data.nobelprize.org/sparql",
       default_graph = "http://data.nobelprize.org",
       prefixes = prefixes,
       base_uri = "http://data.nobelprize.org/"
   )


Definition of a simple function to grasp the result of the query
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   def query_the_world(endpoint, query):
        endpoint = endpoint
        result = endpoint.select(query)
        return result


II. Examples of requests
------------------------

```
Reminder: Triple schema = ?Subjet + ?Object + ?Predicate
```

* How to get Elvis Presley's date of birth from DBPedia?
* Explore the page: [http://dbpedia.org/resource/Elvis_Presley](http://dbpedia.org/resource/Elvis_Presley)
* Find the relevant field: birthdate
* Build a query:

.. code-block::

   PREFIX dbo: <http://dbpedia.org/ontology/>

   SELECT ?bday

   WHERE {
   <http://dbpedia.org/resource/Elvis_Presley> dbo:birthDate ?bday .
   }

* [Request](http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=PREFIX+dbo%3A+%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2F%3E%0D%0A%0D%0ASELECT+%3Fbday+%0D%0A%0D%0AWHERE+%7B%0D%0A++%3Chttp%3A%2F%2Fdbpedia.org%2Fresource%2FElvis_Presley%3E+dbo%3AbirthDate+%3Fbday+.%0D%0A%7D&format=text%2Fhtml&CXML_redir_for_subjs=121&CXML_redir_for_hrefs=&timeout=30000&debug=on&run=+Run+Query+)


Music
~~~~~

* How to find Björk's albums on DBPedia?

.. code-block:: python
   :class: exercise

   bjork_albums_query = """
   SELECT DISTINCT ?album
   WHERE {
     ?album dbpediaowl:musicalArtist <http://dbpedia.org/resource/Björk> .
   }
   """
   query_the_world(DBPEDIA_ENDPOINT, bjork_albums_query).head()


* How to find albums produced by Dr Dre on DBPedia?

  .. code-block:: python
   :class: exercise

   prod_by_dre_query = """
   SELECT DISTINCT ?artist ?album
   WHERE
   {
     ?album dbpediaowl:producer <http://dbpedia.org/resource/Dr._Dre> .
     ?album dbpediaowl:musicalArtist ?artist .
   }
   """
   query_the_world(DBPEDIA_ENDPOINT, prod_by_dre_query).head()


Science
~~~~~~~

* How to find scientists who have an ORCID ID?  
  [ORCID](https://orcid.org/) is a digital identifier applied to
  researchers, research programs and workflows (publications, grants,
  reviews) to ensure a broader recognition of professional activities
  (in academic fields).
* [Request](https://query.wikidata.org/#PREFIX%20bd%3A%20%3Chttp%3A%2F%2Fwww.bigdata.com%2Frdf%23%3E%0APREFIX%20wikibase%3A%20%3Chttp%3A%2F%2Fwikiba.se%2Fontology%23%3E%0APREFIX%20wdt%3A%20%3Chttp%3A%2F%2Fwww.wikidata.org%2Fprop%2Fdirect%2F%3E%0A%0ASELECT%0A%20%20%20%3Fitem%0A%20%20%20%3FitemLabel%0A%20%20%20%23%3FdoctoralAdvisorLabel%0A%20%20%20%3Forcid%0A%20%20%20%3Fdescription%0AWHERE%20%7B%0A%20%20%23%3FitemLabel%20wdt%3AP184%20%3FdoctoralAdvisor%20.%0A%20%20%3Fitem%20wdt%3AP496%20%3Forcid%20.%0A%20%20%20%20%20%20%20%20%0A%20%20BIND%28STRAFTER%28str%28%3Fitem%29%2C%20%22http%3A%2F%2Fwww.wikidata.org%2Fentity%2F%22%29%20as%20%3Fwid%29%0A%20%20OPTIONAL%20%7B%20%3Fitem%20schema%3Adescription%20%3Fdescription%20filter%20%28lang%28%3Fdescription%29%20%3D%20%22en%22%29%20%7D%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%0A%20%20%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22%20.%0A%20%20%7D%0A%7D%20LIMIT%205)

.. code-block:: python
   :class: exercise

   orcid_query = """
   PREFIX bd: <http://www.bigdata.com/rdf#>
   PREFIX wikibase: <http://wikiba.se/ontology#>
   PREFIX wdt: <http://www.wikidata.org/prop/direct/>

   SELECT
      ?item
      ?itemLabel
      #?doctoralAdvisorLabel
      ?orcid
      ?description
   WHERE {
      #?itemLabel wdt:P184 ?doctoralAdvisor .
      ?item wdt:P496 ?orcid .

      BIND(STRAFTER(str(?item), "http://www.wikidata.org/entity/") as ?wid)
      OPTIONAL { ?item schema:description ?description filter (lang(?description) = "en") }
      SERVICE wikibase:label {
           bd:serviceParam wikibase:language "en" .
      }
   } LIMIT 5
   """
   query_the_world(WIKIDATA_ENDPOINT, orcid_query)


Nobel Prize
~~~~~~~~~~~

* How to find people awarded with a Nobel Prize on Wikidata:
  [Request](https://query.wikidata.org/#%23Authors%20of%20scientific%20articles%20who%20received%20a%20Nobel%20prize%0A%23added%20in%202016-10%0A%0A%23Authors%20of%20scientific%20articles%20who%20received%20a%20Nobel%20prize%0ASELECT%20%3Fitem%20%3FitemLabel%20%3Fperson%20%3FpersonLabel%20%3F_image%20%3Faward%20%3FawardLabel%0AWHERE%20%7B%0A%20%20%3Fperson%20wdt%3AP166%20%3Faward%20%3B%20%23person%20received%20an%20award%0A%20%20%20%20%20%20%20%20%20%20wdt%3AP31%20wd%3AQ5%20.%20%23person%20is%20instance%20of%20human%0A%20%20%3Faward%20wdt%3AP31%2Fwdt%3AP279%2a%20wd%3AQ7191%20.%20%23award%20is%20a%20Nobel%20Prize%0A%20%20%3Fitem%20wdt%3AP50%20%3Fperson%20%3B%20%23person%20is%20an%20author%20of%20item%0A%20%20%20%20%20%20%20%20wdt%3AP31%20wd%3AQ13442814%20.%20%23item%20is%20a%20scientific%20article%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%20%7D%0A%20%0AOPTIONAL%20%7B%20%3Fperson%20wdt%3AP18%20%3F_image.%20%7D%20%23Wikimedia%20Commons%20has%20an%20image%20of%20person%0A%7D)


.. code-block:: python
   :class: exercise

   paper_to_nobel_query = """
   #Authors of scientific articles who received a Nobel prize
   #added in 2016-10

   #Authors of scientific articles who received a Nobel prize
   SELECT ?item ?itemLabel ?person ?personLabel ?_image ?award ?awardLabel
   WHERE {
      ?person wdt:P166 ?award ; #person received an award
           wdt:P31 wd:Q5 . #person is instance of human
      ?award wdt:P31/wdt:P279* wd:Q7191 . #award is a Nobel Prize
      ?item wdt:P50 ?person ; #person is an author of item
           wdt:P31 wd:Q13442814 . #item is a scientific article
      SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }

      OPTIONAL { ?person wdt:P18 ?_image. } #Wikimedia Commons has an image of person
   }
   """
   #query_the_world(WIKIDATA_ENDPOINT, paper_to_nobel_query)


.. code-block:: python
   :class: exercise

   endpoint = WIKIDATA_ENDPOINT
   result = endpoint.select(
   """
   SELECT ?item ?itemLabel ?person ?personLabel ?_image ?award ?awardLabel
   WHERE {
     ?person wdt:P166 ?award ; #person received an award
             wdt:P31 wd:Q5 . #person is instance of human
     ?award wdt:P31/wdt:P279* wd:Q7191 . #award is a Nobel Prize
     ?item wdt:P50 ?person ; #person is an author of item
           wdt:P31 wd:Q13442814 . #item is a scientific article
     SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }

   OPTIONAL { ?person wdt:P18 ?_image. } #Wikimedia Commons has an image of person
   }
   """)
   result.head()


* Another way to query:
  [Request](https://query.wikidata.org/#%23People%20that%20received%20Nobel%20Prize%0ASELECT%20DISTINCT%20%3Fperson%20%3FpersonLabel%20%3Fimage%20%3Faward_received%20%3Faward_receivedLabel%20%3Fmember_of%20%3Fmember_ofLabel%20%3Finfluenced_by%20%3Finfluenced_byLabel%20%3Femployer%20%3FemployerLabel%20%3Foccupation%20%3FoccupationLabel%20%3Fsex_or_gender%20%3Fsex_or_genderLabel%20%3Fnotable_work%20%3Fnotable_workLabel%20%3Fpoint_in_time%20WHERE%20%7B%0A%20%20%3Fperson%20wdt%3AP166%20wd%3AQ7191.%0A%20%20%3Fperson%20wdt%3AP21%20%3Fsex_or_gender.%0A%20%20%3Fperson%20wdt%3AP166%20%3Faward_received.%0A%20%20%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fperson%20wdt%3AP463%20%3Fmember_of.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fperson%20wdt%3AP737%20%3Finfluenced_by.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fperson%20wdt%3AP108%20%3Femployer.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fperson%20wdt%3AP800%20%3Fnotable_work.%20%7D%0A%20%20%23OPTIONAL%20%7B%20%3Fperson%20wdt%3AP18%20%3Fimage.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fperson%20wdt%3AP106%20%3Foccupation.%20%7D%0A%20%20%23OPTIONAL%20%7B%20%3Faward_received%20wdt%3AP585%20%3Fpoint_in_time.%C2%A0%7D%0A%7D%20LIMIT%20100%0A)


.. code-block:: python
   :class: exercise

   received_award_query = """
   #People who received Nobel Prize
   SELECT DISTINCT ?person ?personLabel ?image ?award_received ?award_receivedLabel ?member_of ?member_ofLabel ?influenced_by ?influenced_byLabel ?employer ?employerLabel ?occupation ?occupationLabel ?sex_or_gender ?sex_or_genderLabel ?notable_work ?notable_workLabel ?point_in_time WHERE {
      ?person wdt:P166 wd:Q7191.
      ?person wdt:P21 ?sex_or_gender.
      ?person wdt:P166 ?award_received.

      SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
      OPTIONAL { ?person wdt:P463 ?member_of. }
      OPTIONAL { ?person wdt:P737 ?influenced_by. }
      OPTIONAL { ?person wdt:P108 ?employer. }
      OPTIONAL { ?person wdt:P800 ?notable_work. }
      #OPTIONAL { ?person wdt:P18 ?image. }
      OPTIONAL { ?person wdt:P106 ?occupation. }
      #OPTIONAL { ?award_received wdt:P585 ?point_in_time. }
   } LIMIT 10
   """
   query_the_world(WIKIDATA_ENDPOINT, received_award_query).head()


* Women who have been awarded in physics on data.nobelprize.org
  ([source](https://www.nobelprize.org/about/linked-data-examples/)) :
  [Request](http://data.nobelprize.org/snorql/?query=SELECT+DISTINCT+%3Flabel+%0D%0AWHERE+%7B+%0D%0A++%3Flaur+rdf%3Atype+nobel%3ALaureate+.+%0D%0A++%3Flaur+rdfs%3Alabel+%3Flabel+.+%0D%0A++%3Flaur+foaf%3Agender+%22female%22+.+%0D%0A++%3Flaur+nobel%3AlaureateAward+%3Faward+.+%0D%0A++%3Faward+nobel%3Acategory+%3Chttp%3A%2F%2Fdata.nobelprize.org%2Fresource%2Fcategory%2FPhysics%3E+.+%0D%0A%7D)


.. code-block:: python
   :class: exercise

   women_physic_nobel_query = """
   SELECT DISTINCT ?label
   WHERE {
     ?laur rdf:type nobel:Laureate .
     ?laur rdfs:label ?label .
     ?laur foaf:gender "female" .
     ?laur nobel:laureateAward ?award .
     ?award nobel:category <http://data.nobelprize.org/resource/category/Physics> .
   }
   """
   #query_the_world(NOBEL_ENDPOINT, women_physic_nobel_query)

Bnf
~~~

* How to find works from authors?


.. code-block:: python
   :class: exercise

   def get_works_from_author(author_permalink, endpoint = BNF_ENDPOINT):
       result = endpoint.select("""
           SELECT ?work_uri ?title ?date
           WHERE {
               <%(permalink)s> foaf:focus ?author_uri.
               ?work_uri dcterms:creator ?author_uri.
               OPTIONAL { ?work_uri dcterms:title ?title. }
               OPTIONAL { ?work_uri dcterms:date ?date. }
           }
           ORDER BY ?date ?title """ % {"permalink": author_permalink})
       result.set_index("work_uri", inplace=True)
       return result
   get_works_from_author("http://data.bnf.fr/ark:/12148/cb11888266r", BNF_ENDPOINT)
   # Here, Alain Fournier


* You can try with other BNF permalinks:
  * Pierre Bourdieu: http://data.bnf.fr/ark:/12148/cb118934022",
  * Claude Lévi Strauss: http://data.bnf.fr/ark:/12148/cb11912808h",
  * Michel Foucault: http://data.bnf.fr/ark:/12148/cb11903202t"


.. code-block:: python
   :class: exercise

   def display_author_works():
       permalink_input = widgets.Text(
           value="http://data.bnf.fr/ark:/12148/cb118934022",
           placeholder="Identifiant auteur data.bnf.fr",
           description="Auteur : ",
           disabled=False)
       def display_works(author):
           res = get_works_from_author(author)
           display(res)
       table_output = widgets.interactive_output(display_works, {"author": permalink_input})
       return widgets.VBox([permalink_input, table_output])
   display_author_works()

* Scientists trained at EHESS (Ecole des Hautes Etudes en Sciences
  Sociales) and their doctoral advisors (+ other details):


.. code-block:: python
   :class: exercise

   ehess_query = """
   SELECT ?person ?personLabel ?personDescription ?doctoralAdvisorLabel ?birthPlaceLabel ?coordinates ?birthdate ?image
   WHERE {
   #Person educated at EHESS
     ?person wdt:P69 wd:Q273518 .
   #Person whose gender is female
     #?person wdt:P21 wd:Q6581072 .
   #Doctoral Advisor
     ?person wdt:P184 ?doctoralAdvisor .
   #Birthplace
     ?person wdt:P19 ?birthPlace .
     ?birthPlace wdt:P625 ?coordinates .
   #Birthdate
     ?person wdt:P569 ?birthdate .
   #Portrait
     ?person wdt:P18 ?image
         SERVICE wikibase:label {
         bd:serviceParam wikibase:language "fr" .
         }
   }
   """
   query_the_world(WIKIDATA_ENDPOINT, ehess_query).head()


* Same query with a basic html display


.. code-block:: python
   :class: exercise

   def getAuthorsInfo():
      for author in results["results"]["bindings"]:
           portrait = author["image"]["value"]
           display(HTML('<img width="120px" style="" src="'+portrait+'">'))
           name = author["personLabel"]["value"]
           wikidataId = author["person"]["value"]
           description = author.get("personDescription", {}).get("value", "no_data_available")
           language = author["personLabel"]["xml:lang"]
           doctoralAdvisor = author["doctoralAdvisorLabel"]["value"]
           birthDate = author["birthdate"]["value"]
           birthPlace = author["birthPlaceLabel"]["value"]
           coordinates = author["coordinates"]["value"]
           print(name + " : " + description + " (" + language + "), formé par " + doctoralAdvisor + ".\n" + "Id Wikidata : " + wikidataId + ".\n" + "Date de naissance : " + birthDate + ".\n" + "Lieu de naissance : " + birthPlace + ". Coordonnées géographiques : " + coordinates + "\n")
   getAuthorsInfo()


* Scientists trained at the Collège de France and their doctoral
  advisors:


.. code-block:: python
   :class: exercise

   college_de_france_query = """
   SELECT DISTINCT ?person ?personLabel ?personDescription ?doctoralAdvisorLabel WHERE {
   ?person wdt:P69 wd:Q202660.
   ?person wdt:P184 ?doctoralAdvisor.
   SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
   }
   """
   query_the_world(WIKIDATA_ENDPOINT, college_de_france_query).head()


Sports
~~~~~~

* Make some computations on the server side: Get the life expectancy
  per sport: [Request](http://live.dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=prefix+dbp%3A%3Chttp%3A%2F%2Fdbpedia.org%2Fontology%2F%3E%0D%0Aselect+%3FathleteGroupEN+%28count%28%3Fathlete%29+as+%3Fcount%29+%28avg%28%3Fage%29+as+%3FageAvg%29%0D%0Awhere+%7B%0D%0Afilter%28%3Fage+%3E%3D+20+%26%26+%3Fage+%3C%3D+100%29+.%0D%0A%7B%0D%0Aselect+distinct+%3FathleteGroupEN+%3Fathlete+%28%3FdeathYear+-+%3FbirthYear+as+%3Fage%29%0D%0Awhere+%7B%0D%0A%3FsubOfAthlete+rdfs%3AsubClassOf+dbp%3AAthlete+.%0D%0A%3FsubOfAthlete+rdfs%3Alabel+%3FathleteGroup+filter%28lang%28%3FathleteGroup%29+%3D+%22en%22%29+.%0D%0Abind%28str%28%3FathleteGroup%29+as+%3FathleteGroupEN%29%0D%0A%3Fathlete+a+%3FsubOfAthlete+.%0D%0A%3Fathlete+dbp%3AbirthDate+%3Fbirth+filter%28datatype%28%3Fbirth%29+%3D+xsd%3Adate%29+.%0D%0A%3Fathlete+dbp%3AdeathDate+%3Fdeath+filter%28datatype%28%3Fdeath%29+%3D+xsd%3Adate%29+.%0D%0Abind+%28strdt%28replace%28%3Fbirth%2C%22%5E%28%5C%5Cd%2B%29-.*%22%2C%22%241%22%29%2Cxsd%3Ainteger%29+as+%3FbirthYear%29+.%0D%0Abind+%28strdt%28replace%28%3Fdeath%2C%22%5E%28%5C%5Cd%2B%29-.*%22%2C%22%241%22%29%2Cxsd%3Ainteger%29+as+%3FdeathYear%29+.%0D%0A%7D%0D%0A%7D%0D%0A%7D+group+by+%3FathleteGroupEN+having+%28count%28%3Fathlete%29+%3E%3D+25%29+order+by+%3FageAvg&format=text%2Fhtml&CXML_redir_for_subjs=121&CXML_redir_for_hrefs=&timeout=30000&signal_void=on&signal_unconnected=on&run=+Run+Query+)([Source](https://semantic-web.com/2015/09/29/sparql-analytics-proves-boxers-live-dangerously/))

.. code-block:: python
   :class: exercise

   sport_life_expectancy_query = """
   select ?athleteGroupEN (count(?athlete) as ?count) (avg(?age) as ?ageAvg)
   where {
   filter(?age >= 20 && ?age <= 100) .
   {
   select distinct ?athleteGroupEN ?athlete (?deathYear - ?birthYear as ?age)
   where {
   ?subOfAthlete rdfs:subClassOf dbpediaowl:Athlete .
   ?subOfAthlete rdfs:label ?athleteGroup filter(lang(?athleteGroup) = "en") .
   bind(str(?athleteGroup) as ?athleteGroupEN)
   ?athlete a ?subOfAthlete .
   ?athlete dbpediaowl:birthDate ?birth filter(datatype(?birth) = xsd:date) .
   ?athlete dbpediaowl:deathDate ?death filter(datatype(?death) = xsd:date) .
   bind (strdt(replace(?birth,"^(\\d+)-.*","$1"),xsd:integer) as ?birthYear) .
   bind (strdt(replace(?death,"^(\\d+)-.*","$1"),xsd:integer) as ?deathYear) .
   }
   }
   } group by ?athleteGroupEN having (count(?athlete) >= 25) order by ?ageAvg
   """
   #query_the_world(DBPEDIA_ENDPOINT, sport_life_expectancy_query)


Discontinued Google products
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[Timeline here](https://query.wikidata.org/#%23defaultView%3ATimeline%0ASELECT%20DISTINCT%20%3Fitem%20%3FitemLabel%20%3Fdiscontinued%20%3Flogo%0AWHERE%20%0A%7B%0A%20%20%3Fitem%20wdt%3AP31%2Fwdt%3AP279%2a%20wd%3AQ7397.%0A%20%20%3Fitem%20wdt%3AP2669%20%3Fdiscontinued.%0A%20%20%7B%3Fitem%20wdt%3AP178%20wd%3AQ95%7D%0A%20%20UNION%20%7B%3Fitem%20wdt%3AP127%20wd%3AQ95%7D.%0A%20%20OPTIONAL%20%7B%3Fitem%20wdt%3AP154%20%3Flogo%7D.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A%7D)

.. code-block:: python
   :class: exercise

   discontinued_gproducts_query = """
   #defaultView:Timeline
   SELECT DISTINCT ?item ?itemLabel ?discontinued ?logo
   WHERE 
   {
     ?item wdt:P31/wdt:P279* wd:Q7397.
     ?item wdt:P2669 ?discontinued.
     {?item wdt:P178 wd:Q95}
     UNION {?item wdt:P127 wd:Q95}.
     OPTIONAL {?item wdt:P154 ?logo}.
     SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
   }
   """
   query_the_world(WIKIDATA_ENDPOINT, discontinued_gproducts_query)
