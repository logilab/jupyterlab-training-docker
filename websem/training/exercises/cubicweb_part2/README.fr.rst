**Jouons avec une instance de formation**
=========================================

Toujours dans le terminal

<sub>**! ATTENTION** Si vous éxécutez ce tutoriel sur votre poste, travaillez dans un environnement virtuel (python3 -m venv cubicenv && . cubicenv/bin/activate)</sub>  


Création d'un cube *training_cube*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

```
$> cubicweb-ctl newcube training_cube
```

Création d'une instance *training_instance* du cube *training_cube*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

```
$> cd cubicweb-training-cube
$> cubicweb-ctl create training_cube training_instance
```

Choisir **sqlite** pour **:db-driver:** et renseigner le mot de passe.  
Garder les paramètres par défaut pour les autres questions.

Lancement de l'instance *training_instance*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lancer l'instance 

```
$> cubicweb-ctl pyramid -Dlinfo training_instance
```

puis connecter vous via votre navigateur à l'adresse: http://localhost:8080

Ajout d'un schema
~~~~~~~~~~~~~~~~~

Maintenant que nous avons notre instance nous allons la personaliser en lui ajoutant un schema (base de données).

Arrêtons l'instance qui tourne dans le terminal avec un `Ctrl-c`, puis éditons le fichier `cubicweb-training-cube/cubicweb_training_cube/schema.py` dans l'éditeur de jupyterlab.

**Astuce**: Il est possible de changer le keybinding par défaut de l'éditeur dans `Settings`

Ensuite, copier/coller le code si dessous dans le fichier schema.py:

.. literalinclude:: /cubicweb_part2/schema.py
                    :language: python

Puis recréer la base de données:

```
$> cubicweb-ctl db-create training_instance
```

Relancer l'instance

```
$> cubicweb-ctl pyramid -Dlinfo training_instance
```

Puis se reconnecter pour voir le nouveau schéma à l'adresse: http://localhost:8080

Regarder en particulier la nouvelle table `Session`
