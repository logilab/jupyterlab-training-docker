**Jouons avec une instance de formation**
=========================================

Toujours dans le terminal

<sub>**! ATTENTION** Si vous éxécutez ce tutoriel sur votre poste, travaillez dans un environnement virtuel (python3 -m venv cubicenv && . cubicenv/bin/activate)</sub>  


Création d'un cube *training_cube*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. highlightlang:: bash
$> cubicweb-ctl newcube training_cube


Création d'une instance *training_instance* du cube *training_cube*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

````
$> cd cubicweb-training-cube
$> cubicweb-ctl create training_cube training_instance
````

Choisir **sqlite** pour **:db-driver:** et renseigner le mot de passe.  
Garder les paramètres par défaut pour les autres questions.

Lancement de l'instance *training_instance*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lancer l'instance 

````
$> cubicweb-ctl pyramid -Dlinfo training_instance
````

puis connecter vous via votre navigateur à l'adresse: http://localhost:8080

Ajout d'un schema
~~~~~~~~~~~~~~~~~

Maintenant que nous avons notre instance nous allons la personaliser en lui ajoutant un schema (base de données).

Arrêtons l'instance qui tourne dans le terminal avec un `Ctrl-c`, puis éditons le fichier `cubicweb-training-cube/cubicweb_training_cube/schema.py` dans l'éditeur de jupyterlab.

**Astuce**: Il est possible de changer le keybinding par défaut dans `Settings`

Ensuite, copier/coller le code si dessous dans le fichier:

````  python
from yams.buildobjs import Boolean, EntityType, Float, RelationDefinition, String
from cubicweb.schema import RRQLExpression

SESSION_TYPES = ['cubicweb', 'python', 'semantic_web']

class Session(EntityType):
    session_type = String(required=True, vocabulary=SESSION_TYPES, default=SESSION_TYPES[0])
    location = String(required=True)
    duration = Float(required=True, default=8, description='Hours')
    is_opened = Boolean(default=False, description='is the session in progress ?')

class participated_in(RelationDefinition):
    __permissions__ = {
        'read': ('managers', 'users'),
        'add': ('managers', RRQLExpression('O owned_by U')),
        'delete': ('managers', RRQLExpression('O owned_by U')),
    }
    subject = 'CWUser'
    object = ('Session')
    cardinality = '**'
````

Puis recréer la base de données:

````
$> cubicweb-ctl db-create training_instance
````

Relancer l'instance

````
$> cubicweb-ctl pyramid -Dlinfo training_instance
````

Puis se reconnecter pour voir le nouveau schéma à l'adresse: http://localhost:8080  
Regarder en particulier la nouvelle table `Session`
