from cubicweb.schema import RRQLExpression
from yams.buildobjs import (
    Boolean,
    EntityType,
    Float,
    RelationDefinition,
    String,
)

SESSION_TYPES = ['cubicweb', 'python', 'semantic_web']


class Session(EntityType):
    session_type = String(
        required=True, vocabulary=SESSION_TYPES, default=SESSION_TYPES[0]
    )
    location = String(required=True)
    duration = Float(required=True, default=8, description='Hours')
    is_opened = Boolean(
        default=False, description='is the session in progress ?'
    )


class participated_in(RelationDefinition):
    __permissions__ = {
        'read': ('managers', 'users'),
        'add': ('managers', RRQLExpression('O owned_by U')),
        'delete': ('managers', RRQLExpression('O owned_by U')),
    }
    subject = 'CWUser'
    object = ('Session')
    cardinality = '**'
