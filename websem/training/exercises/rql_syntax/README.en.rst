**RQL introduction**
====================

After launch CubicWeb from the launcher, write RQL queries to query for the following tables:

![tables](usertable.png)


Get the id of all Sessions
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_1.rql
                    :language: rql
                    :class: solution


Get id and login of all users
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_2.rql
                    :language: rql
                    :class: solution


Get the locations (location) of the session the trainee 'abeng
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_3.rql
                    :language: rql
                    :class: solution


RGet the firstnames of the users 'jcole' and 'jkhol
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_4.rql
                    :language: rql
                    :class: solution



Recover user names that end with 'ty'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_5.rql
                    :language: rql
                    :class: solution



Collect the user names that contain an 'e' and 'a'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

 # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_6.rql
                    :language: rql
                    :class: solution



Get the names of organizations that finisent with a digit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_7.rql
                    :language: rql
                    :class: solution



Get the names of trainers (sorted by name) for each session has one
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_8.rql
                    :language: rql
                    :class: solution


Recover user names of employees of the organization 'TheOrg' and having an email address
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_9.rql
                    :language: rql
                    :class: solution


Get the names of the user belonging to the organization 'org1' and participating in a session 'python'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Your RQL Code


.. literalinclude:: /rql_syntax/rql_syntax_solution_10.rql
                    :language: rql
                    :class: solution


