**Introduction à RQL**
======================

Après avoir lancer cubicweb depuis le launcher, écrire les requêtes RQL permettant de requêter les
tables suivantes:

![tables](usertable.png)


Récupérer les id de toutes les Sessions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_1.rql
                    :language: rql
                    :class: solution


Récupérer les ids et les logins des utilisateurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_2.rql
                    :language: rql
                    :class: solution


Récupérer le lieux (location) de la session du stagiaire 'abeng'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_3.rql
                    :language: rql
                    :class: solution


Récupérer les prénoms (firstname) des utilisateurs 'jcole' et 'jkhol'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_4.rql
                    :language: rql
                    :class: solution



Récupérer les prénoms des utilisateurs qui finissent par 'ty'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_5.rql
                    :language: rql
                    :class: solution



Récupérer les noms des utilisateurs qui contiennent un 'e' et un 'a'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

 # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_6.rql
                    :language: rql
                    :class: solution



Récupérer les noms des organisations qui finissent par un chiffre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_7.rql
                    :language: rql
                    :class: solution



Récupérer les noms des formateurs (trié par nom) pour chaque session qui en possède un
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_8.rql
                    :language: rql
                    :class: solution


Récupérer les prénoms des utilisateur employés de l'organisation 'TheOrg' et ayant une adresse mail
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_9.rql
                    :language: rql
                    :class: solution


Récupérer les noms des utilisateur appartenant à l'organisation 'org1' et participant à une session 'python'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: rql
   :class: exercise

   # Votre Code RQL


.. literalinclude:: /rql_syntax/rql_syntax_solution_10.rql
                    :language: rql
                    :class: solution


