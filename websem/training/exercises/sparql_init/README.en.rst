
Using the namespace foaf, accessible `here <http://xmlns.com/foaf/0.1/>`_ , create a new person.

Create a sparql query that return a Person (in foaf sense) that has Eric as firstName (in foaf sense)
**hint**: Variable name start with ``?``.

.. code-block:: python
   :class: exercise

   sparql_query = """
   Select
   Where {

   }
   """

.. literalinclude:: /exos/modules/Web/sem/sparql_init/sparql_init_unittests.py
                    :language: python
                    :class: test

**Solution**

.. literalinclude:: /exos/modules/Web/sem/sparql_init/sparql_init_solutions.py
                    :language: python
                    :class: solution
