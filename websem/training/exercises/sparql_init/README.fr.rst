

Créer une requête sparql qui retourne une Personne (dans le sense de ``foaf``)
qui a Eric comme prénom (propriété firstName dans foaf ).
**Indice**: Les variables sparql sont préfixées par ``?``.

.. code-block:: python
   :class: exercise

   sparql_query = """
   Select
   Where {

   }
   """

.. literalinclude:: /exos/modules/Web/sem/sparql_init/sparql_init_unittests.py
                    :language: python
                    :class: test

**Solution**

.. literalinclude:: /exos/modules/Web/sem/sparql_init/sparql_init_solutions.py
                    :language: python
                    :class: solution
