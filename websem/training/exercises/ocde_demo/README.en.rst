
Life expectancy exploration
---------------------------

Source OECD: https://stats.oecd.org/Index.aspx?DatasetCode=HEALTH_STAT

Imports
~~~~~~~

.. code-block:: python
   :class: exercise

   import pandas as pda
   from pandas import pivot_table
   import numpy as np
   import matplotlib
   import matplotlib.pyplot as plt
   from matplotlib import style
   import ipywidgets as widgets
   from ipywidgets import interact, interactive

Dataset
~~~~~~~

.. code-block:: python
   :class: exercise

   df = pda.read_csv("data/HEALTH_STAT_03072018162953866.csv", index_col=0)
   df = df.fillna(0)
   df.head()


Check data consistency
~~~~~~~~~~~~~~~~~~~~~~

* The tail indicates incomplete data

.. code-block:: python
   :class: exercise

   df.Country.value_counts().tail(7)


* Variables without values despite the fillna?

.. code-block:: python
   :class: exercise

   df.isnull().sum()


* Cleaning

.. code-block:: python
   :class: exercise

   df = df.drop(["Measure", "COU", "YEA","Flag Codes", "Flags"], axis=1)
   df.head(1)


Overview
--------

Top 20
~~~~~~

* All variables

.. code-block:: python
   :class: exercise

   top_20_all = df.nlargest(20, "Value")[ ["Country", "Variable", "Year", "Value"] ]
   top_20_all


* Total population

.. code-block:: python
   :class: exercise

   top_20_tot = df.loc[ ["EVIETOTA"] ]
   top_20_tot = top_20_tot.nlargest(20, "Value")[ ["Country", "Variable", "Year", "Value"] ]
   top_20_tot


* Tail of the ranking

.. code-block:: python
   :class: exercise

   last = df.loc[ ["EVIETOTA"] ]
   last = last.nsmallest(20, "Value")[ ["Country", "Variable", "Year", "Value"] ]
   last


Plot one country
~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   def plot_one_country(country):
      country_to_plot = df[ (df["Country"] == country) & (df["UNIT"] == "EVIDUREV")]
      country_to_plot_tot = country_to_plot.loc[ ["EVIETOTA"] ]
      country_to_plot_tot.set_index("Year").plot(title = country + ": General Population")
      return country
   i = interact(plot_one_country, country = sorted(df["Country"].unique()))


Plot several countries with a selected variable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   def to_plot(countries, variable):
      country_to_plot = df[ (df["Country"].isin(countries)) & (df["UNIT"] == "EVIDUREV") ]
      country_to_plot = country_to_plot[country_to_plot["Variable"] == variable ]
      for idx, country in enumerate(countries):
           selected_countries = country_to_plot[country_to_plot["Country"] == country]
           selected_countries = pda.DataFrame({"x": selected_countries["Year"], "y": selected_countries["Value"]}).set_index("x")
           plt.figure(1, figsize=(15,10))
           plt.plot(selected_countries, label = country)
           leg = plt.legend(loc='upper left', ncol=2, mode="expand")
           leg.get_frame().set_alpha(0.5)
      plt.show()

   i = interact(to_plot, countries = widgets.SelectMultiple(options = sorted(df["Country"].unique())), variable = sorted( (v for v in df["Variable"].unique() if "at birth" in v) ))


Means
~~~~~

.. code-block:: python
   :class: exercise

   quick_mean = df[df["UNIT"] == "EVIDUREV"]
   quick_mean = quick_mean.loc[ ["EVIETOTA"] ].set_index("Country")
   quick_mean = quick_mean.groupby(['Country']).mean()['Value']
   quick_mean.sort_values(ascending=False).head()


.. code-block:: python
   :class: exercise

   means = df[df["UNIT"] == "EVIDUREV"]
   table = pivot_table(means, values = ["Value"], index = ["Year"], columns = ["Variable"], aggfunc = np.mean)
   table


.. code-block:: python
   :class: exercise

   table.plot(figsize=(15, 15))
   pass


.. code-block:: python
   :class: exercise

   table.plot(figsize=(15, 15), subplots=True)
   pass


* Life expectancy for total populations accross time and countries

.. code-block:: python
   :class: exercise

   means_tot_at_birth = df[ (df["UNIT"] == "EVIDUREV") & (df["Variable"] == "Total population at birth") ]
   table = pivot_table(means_tot_at_birth, values = ["Value"], index = ["Year"], columns = ["Country"], aggfunc = np.mean)
   table


.. code-block:: python
   :class: exercise

   table.plot(figsize=(15, 15))
   pass


Details
-------

Japan
~~~~~

* Selection

.. code-block:: python
   :class: exercise

   jpn = df[ (df["Country"] == "Japan") & (df["UNIT"] == "EVIDUREV")]
   jpn.head()


* General population

.. code-block:: python
   :class: exercise

   jpn_tot = jpn.loc[["EVIETOTA"]]
   jpn_tot_to_plot = jpn_tot.set_index("Year").plot(title="Japan: General population", color="green")
   jpn_tot_to_plot.legend(["Life expectancy"])
   pass


* Female population

.. code-block:: python
   :class: exercise

   jpn_fem = jpn.loc[ ["EVIEFE00"] ]
   jpn_fem_to_plot = jpn_fem.set_index("Year").plot(title="Japan: Female population", color="red")
   jpn_fem_to_plot.legend(["Life expectancy"])
   pass


* Male population

.. code-block:: python
   :class: exercise

   jpn_masc = jpn.loc[ ["EVIEHO00"] ]
   jpn_masc_to_plot = jpn_masc.set_index("Year").plot(title="Japan: Male population", color="blue")
   jpn_masc_to_plot.legend(["Life expectancy"])
   pass


* Combined view

.. code-block:: python
   :class: exercise

   #from matplotlib import pyplot as plt
   style.use('fivethirtyeight')
   fig = plt.figure()

   ax1 = fig.add_subplot(221)
   ax2 = fig.add_subplot(222)
   ax3 = fig.add_subplot(212)

   # Female
   ax1.plot(jpn_fem["Year"],jpn_fem["Value"], color="r", label="Female")
   ax1.legend(loc="upper left")

   # Male
   ax2.plot(jpn_masc["Year"],jpn_masc["Value"], color="b", label="Male")
   ax2.legend(loc="upper left")

   # General
   ax3.plot(jpn_tot["Year"],jpn_tot["Value"], color="g", label="General")
   ax3.legend(loc="upper left")
   pass


.. code-block:: python
   :class: exercise

   # from matplotlib import pyplot as plt
   jpn_full = jpn.loc[["EVIETOTA", "EVIEFE00", "EVIEHO00"]]
   fig, ax = plt.subplots(figsize=(15,7))
   plap = jpn_full.set_index('Year').groupby(['Variable']).plot(ax=ax, title="Life Expectancy: Japan")
   ax.legend(["Female at birth", "Male at birth", "General population at birth"])      
   jpn_full.sort_values(by="Value", ascending=False).head()


South Africa
~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   sa = df[ (df["Country"] == "South Africa") & (df["UNIT"] == "EVIDUREV")]
   sa_tot = sa.loc[ ["EVIETOTA"] ]
   sa_fem = sa.loc[ ["EVIEFE00"] ]
   sa_masc = sa.loc[ ["EVIEHO00"] ]


.. code-block:: python
   :class: exercise

   # from matplotlib import pyplot as plt

   style.use('fivethirtyeight')
   fig = plt.figure()

   for i in range(10):
      x = df["Year"]
      y = df["Value"]

   ax1 = fig.add_subplot(221)
   ax2 = fig.add_subplot(222)
   ax3 = fig.add_subplot(212)

   # Female
   ax1.plot(sa_fem["Year"],sa_fem["Value"], color="r", label="Female")
   ax1.legend(loc="upper left")

   # Male
   ax2.plot(sa_masc["Year"],sa_masc["Value"], color="b", label="Male")
   ax2.legend(loc="upper left")

   # General
   ax3.plot(sa_tot["Year"],sa_tot["Value"], color="g", label="General")
   ax3.legend(loc="upper left")
   pass


.. code-block:: python
   :class: exercise

   #from matplotlib import pyplot as plt
   sa_full = sa.loc[["EVIETOTA", "EVIEFE00", "EVIEHO00"]]
   fig, ax = plt.subplots(figsize=(15,7))
   plip = sa_full.set_index('Year').groupby(['Variable']).plot(ax=ax, title="Life Expectancy: Japan")
   ax.legend(["Female at birth", "Male at birth", "General population at birth"])
   sa_full.sort_values(by="Value", ascending=False).head()


Comparison Japan / South Africa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :class: exercise

   #from matplotlib import pyplot as plt
   style.use('fivethirtyeight')
   fig = plt.figure()

   ax1 = fig.add_subplot(211) # 2 by 1 & plot number 1
   ax2 = fig.add_subplot(212) # 2 by 1 & plot number 2

   #Japan
   ax1.plot(jpn_tot["Year"],jpn_tot["Value"], color = "b", label="Japan")
   ax1.legend(loc="upper left")
   #South Africa
   ax2.plot(sa_tot["Year"],sa_tot["Value"], color ="r", label="South Africa")
   ax2.legend(loc="upper left")
   pass


.. code-block:: python
   :class: exercise

   from matplotlib import pyplot as plt
   selecta = df[ (df["Country"] == "Japan") | (df["Country"] == "South Africa") & (df["UNIT"] == "EVIDUREV") ]
   selecta = selecta.loc[["EVIETOTA"]]
   fig, ax = plt.subplots(figsize=(15,7))

   ploup = selecta.set_index('Year').groupby(['Country']).plot(ax=ax, title="Life Expectancy Comparison (total population at birth)")
   ax.legend(["Japan", "South Africa"])
   selecta.sort_values(by="Value", ascending=True).head()


* Difference between genders

.. code-block:: python
   :class: exercise

   diff_mean = df [ df["UNIT"] == "EVIFHOEV" ]
   diff_mean = diff_mean.loc[ ["EVIEFE00"] ].set_index("Country")
   diff_mean = diff_mean.groupby(['Country']).mean()['Value']
   diff_mean.sort_values(ascending=False).head()


.. code-block:: python
   :class: exercise

   df[df["UNIT"] == "EVIFHOEV"].nlargest(10, "Value")[ ["Country", "Variable", "Year", "Value"] ]


.. code-block:: python
   :class: exercise

   ru = df[ (df["Country"] == "Russia") & (df["UNIT"] == "EVIFHOEV")]
   ru_diff = ru.loc[ ["EVIEFE00"] ]
   ru_diff = ru_diff.set_index("Year").plot(title="Difference between genders", color="green")
   ru_diff.legend(["Difference"])
   pass
