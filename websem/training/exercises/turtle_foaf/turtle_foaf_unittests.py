# formation_unittest
"""Check descriptors exercise."""
import unittest
from jupyterlab_training import FormUnitTests


class TurtlePersonTest(unittest.TestCase):
    def test_person(self):
        import rdflib
        g = rdflib.Graph()
        try:
            g.parse(data=turtle_triple, format="turtle")
        except:
            print("The triple is in the wrong format.")
            print("Did you run the cell containing turtle_triple?")
            self.fail("The triple is in the wrong format.")
        qres = g.query("""SELECT ?a
        WHERE {
          ?a a foaf:Person .
        }""")

        res = {r[0] for r in qres}
        true_res = rdflib.term.URIRef('http://example.com#Alice')
        if true_res not in res:
            print("Your triples does not contains a foaf:Person.")
            print("It contains:\n", g.serialize(format="n3").decode())
            self.fail("Wrong result")



# formation widget
FormUnitTests(TurtlePersonTest);
