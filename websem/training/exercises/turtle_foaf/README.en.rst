
Using the namespace foaf, accessible `here <http://xmlns.com/foaf/0.1/>`_ , create a new person.

The person should have ``#Alice`` as identifier and use the class ``Person`` from foaf.

**hint**: Use '<' and'>' to define an URI.

.. code-block:: python
   :class: exercise

   turtle_triple = """@base <http://example.com> .
   @prefix foaf: <http://xmlns.com/foaf/0.1/> .
   # Write your triple here


   """

.. literalinclude:: /exos/modules/Web/sem/turtle_foaf/turtle_foaf_unittests.py
                    :language: python
                    :class: test

**Solution**

.. literalinclude:: /exos/modules/Web/sem/turtle_foaf/turtle_foaf_solutions.py
                    :language: python
                    :class: solution
