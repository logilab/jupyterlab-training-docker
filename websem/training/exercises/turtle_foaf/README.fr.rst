
Créer une personne en utilisant l'espace de nom ``foaf`` accessible `ici <http://xmlns.com/foaf/0.1/>`_.
La personne doit avoir ``#Alice`` comme identifiant et appartenir à la classe ``Person`` de ``foaf``.

**Indice**: Utiliser ``<`` et ``>`` pour définir une URI.

.. code-block:: python
   :class: exercise

   turtle_triple = """@base <http://example.com> .
   @prefix foaf: <http://xmlns.com/foaf/0.1/> .
   # Écrire le triplet ici


   """

.. literalinclude:: /exos/modules/Web/sem/turtle_foaf/turtle_foaf_unittests.py
                    :language: python
                    :class: test

**Solution**

.. literalinclude:: /exos/modules/Web/sem/turtle_foaf/turtle_foaf_solutions.py
                    :language: python
                    :class: solution
