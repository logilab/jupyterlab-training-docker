# Using your custom ontology

Now that you developed your own weather ontology, let's use this ontology to describe the previous deployment.

## Describing the devices

First, let us update the semantic descriptors attached to sensors.


.. code-block:: python
   :class: exercise

   from om2m_resources.resource import cse, ae, cnt, cin, sub, sgn, smd
   from om2m_client import OM2MClient
   import json, base64, rdflib, requests

   # Run this piece of code to load the global variables in the environment
   OM2M_URL = "http://localhost:8082/~"
   CSE_ID = "/in-cse/"
   CSE_NAME = "in-name"
   LOGIN="admin"
   PSWD="admin"
   OM2M_BASE = OM2M_URL+CSE_ID
   auth_headers = {"X-M2M-ORIGIN":LOGIN+":"+PSWD}
   # The other accepted value is application/xml
   common_headers = {"Accept": "application/json"}
   test_client = OM2MClient(OM2M_URL, CSE_ID, 4568)


.. code-block:: python
   :class: exercise

   # Use the following to delete resources
   target = "http://localhost:8082/~/in-cse/smd-262092804"
   header_smd = {"Content-Type":"application/json;ty=24"}
   esponse = requests.delete(target, headers={**auth_headers, **common_headers, **header_smd})

.. code-block:: python
   :class: exercise

   header_smd = {"Content-Type":"application/json;ty=24"}
   data = smd()
   # Let us successively update the descriptors of the weather station sensors

   # First, the station itself
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/STATION_DESCRIPTOR"
   data.dsp=None # Here, add your own descriptor
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_smd})

   # Then, the anemometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Anemometer/ANEMOMETER_DESCRIPTOR"
   data.dsp=None # Here, add your own descriptor
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_smd})

   # Then, the thermometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Thermometer/THERMOMETER_DESCRIPTOR"
   data.dsp=None # Here, add your own descriptor
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_smd})

   # Finally, the barometer
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Barometer/BAROMETER_DESCRIPTOR"
   data.dsp=None # Here, add your own descriptor
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_smd})


## Enriching the data

Now that the nodes are described, let us enrich the content.
Each time an observation is produced by the IPE, it is stored as a raw value in a content instance.
However, these raw values are quite hard to reuse, because they do not embed their own context.
In order to increase their reusability, we will make it so that the IPE, upon the production of an observation,
both stores its raw value and a descriptor for this value describing the observation with an ontology.


Here is an example of enriched observation, based on SOSA:

.. code-block:: raw

   ex:obs001 rdf:type sosa:Observation ;
     sosa:observedProperty  ex:windSpeed ;
     sosa:madeBySensor ex:anemometer ;
     sosa:hasResult [
       rdf:type qudt-1-1:QuantityValue ;
       qudt-1-1:numericValue "22.4"^^xsd:double ;
       qudt-1-1:unit qudt-unit-1-1:KilometerPerSecond ] ;
     sosa:resultTime "2017-04-16T00:00:12+00:00"^^xsd:dateTimeStamp .


Below are some example SPARQL queries that might be of use for enrichment purpose.

.. code-block:: python
   :class: exercise

   extract_foi_query="""
   PREFIX sosa: <http://www.w3.org/ns/sosa/>
   SELECT ?sensor ?feature
   WHERE {
      ?sensor a sosa:Sensor;
          sosa:observes ?feature.
   }
   """

.. code-block:: python
   :class: exercise

   from sensors import Barometer, Anemometer, TemperatureSensor, LightSensor

   class Weather_Station():
       def __init__(self):
           self.barometer = Barometer("MyBarometer")
           self.anemometer= Anemometer("MyAnemometer")
           self.temperature= TemperatureSensor("MyThermometer")

   class Room():
       def __init__(self, name):
           self.name = name
           self.temperature= TemperatureSensor(name+"Temperature")
           self.luminosity = LightSensor(name+"Light")

   class ADREAM_IPE():
       def __init__(self):
           self.weather = Weather_Station()
           self.rooms = []
           for room in ["H101, H102, H103"]:
               self.rooms.append(Room(room))
           self.client = OM2MClient(OM2M_URL, CSE_ID, 4567)
           # The parameters are: parent name, resource name, and application ID.
           self.weather_ae = self.client.create_ae(CSE_ID+CSE_NAME, "Weather_Station", "Adream control")
           self.weather_ae_name = CSE_ID+CSE_NAME+"/Weather_Station"
           self.comfort_ae = None # Now, create an AE for the comfort part of the Adream application
           self.comfort_ae_name = None
           self.initialize_weather(self.weather_ae_name)
           self.initialize_comfort(self.comfort_ae_name)

       def initialize_weather(self, ae_name):
           self.client.create_cnt(ae_name, "Anemometer")
           self.client.create_cnt(ae_name+"/Anemometer", "DATA")
           self.client.create_cnt(ae_name, "Thermometer")
           self.client.create_cnt(ae_name+"/Thermometer", "DATA")
           self.client.create_cnt(ae_name, "Barometer")
           self.client.create_cnt(ae_name+"/Barometer", "DATA")

       def initialize_comfort(self, ae_name):
           # Create the Containers for the rooms and their sensors
           None

       def enrich_value(self, sensor_descriptor, value):
           g=rdflib.Graph()
           g.parse(data=sensor_descriptor, format="xml")
           qres = g.query(extract_foi_query)
           for row in qres:
               print("{0} observes {1}".format(row))
           # Based on your ontology and the extracted information about the sensor,
           # build a semantic description of the observation. The descriptor should be in XML
           descriptor = None
           return descriptor

       def read_values(self):
           # New pressure observation
           pressure = self.weather.barometer.read_Value()
           pressure_cin = self.client.create_cin(self.weather_ae_name+"/Barometer/DATA", pressure)
           sensor_desc_name = self.weather_ae_name+"/Barometer/BAROMETER_DESCRIPTOR"
           sensor_smd = smd(json_obj=requests.get(OM2M_URL+sensor_desc_name, headers={**auth_headers, **common_headers}).text)
           enriched_obs = self.enrich_value(base64.b64decode(sensor_smd.dsp).decode("utf-8"), pressure)
           # The new observation is connected to the sensor that produced it with related semantics
           response = test_client.create_smd(pressure_cin.ri, base64.b64encode(enriched_obs).decode("utf-8"), rels=sensor_desc_name)

           # New temperature observation
           temperature = self.weather.temperature.read()
           sensor_desc_name = self.weather_ae_name+"/Thermometer/THERMOMETER_DESCRIPTOR"
           sensor_smd = smd(json_obj=requests.get(OM2M_URL+sensor_desc_name, headers={**auth_headers, **common_headers}).text)
           temperature_cin = self.client.create_cin(self.weather_ae_name+"/Thermometer/DATA", temperature)
           enriched_obs = self.enrich_value(base64.b64decode(sensor_smd.dsp).decode("utf-8"), pressure)
           response = test_client.create_smd(temperature_cin.ri, base64.b64encode(enriched_obs).decode("utf-8"), rels=sensor_desc_name)

           # New wind speed observation
           wind_speed = self.weather.anemometer.value()
           sensor_desc_name = self.weather_ae_name+"/Anemometer/ANEMOMETER_DESCRIPTOR"
           sensor_smd = smd(json_obj=requests.get(OM2M_URL+sensor_desc_name, headers={**auth_headers, **common_headers}).text)
           wind_speed_cin =self.client.create_cin(self.weather_ae_name+"/Anemometer/DATA", wind_speed)
           enriched_obs = self.enrich_value(base64.b64decode(sensor_smd.dsp).decode("utf-8"), pressure)
           response = test_client.create_smd(temperature_cin.ri, base64.b64encode(enriched_obs).decode("utf-8"), rels=sensor_desc_name)
           # Add the reading for the rooms sensors

   #del(ipe)
   ipe = ADREAM_IPE()


.. code-block:: python
   :class: exercise

   ipe.read_values()


## Performing online analysis

Embedding descriptions in the sensors and observations enable to perform analysis within queries sent to the platform.
For instance, let us consider that the nominal operating conditions of a device are embedded in its description.
Such description may be done thanks to SSN-Systems, an SSN/SOSA extension, as shown in https://www.w3.org/TR/vocab-ssn/#dht22-description.

In this case, it is possible to express a query testing if a sensor
observed a value outside of its nominal operation conditions, and to use it for discovery.
The following snippet of code updates the descriptor of the anemometer
in order to specify it should measure winds between 0 and 100 Km/H.
Then, an observation is added (along with a semantic descriptor) specifying an observation of 130Km/H.


.. code-block:: python
   :class: exercise

   header_smd = {"Content-Type":"application/json;ty=24"}
   data = smd()

   # First, let's update the anemometer descriptor
   g=rdflib.Graph()
   g.parse("ttl/anemometer_conditions.ttl", format="ttl")
   target = OM2M_BASE+CSE_NAME+"/Weather_Station/Anemometer/ANEMOMETER_DESCRIPTOR"
   data.dsp=base64.b64encode(g.serialize(format='xml')).decode("utf-8")
   response = requests.put(target, data=data.serialize(), headers={**auth_headers, **common_headers, **header_smd})

   # Then, let's create the observation and its descriptor
   strong_wind_cin = test_client.create_cin(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/DATA", 90)
   g=rdflib.Graph()
   g.parse("ttl/wind_observation.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/DATA",
                                     base64.b64encode(g.serialize(format='xml')).decode("utf-8"),
                                     rels=CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/ANEMOMETER_DESCRIPTOR")


.. code-block:: python
   :class: exercise

   find_damaged_sensors = """
   PREFIX sosa: <http://www.w3.org/ns/sosa/>
   PREFIX qudt-1-1: <http://qudt.org/1.1/schema/qudt#>
   PREFIX ssn-system: <http://www.w3.org/ns/ssn/systems/>
   PREFIX schema: <http://schema.org/>
   SELECT *
   WHERE {
       ?observation a sosa:Observation;
           sosa:madeBySensor ?sensor ;
           sosa:hasResult [
               qudt-1-1:numericValue ?val;
           ].
       ?sensor ssn-system:hasOperatingRange [
           ssn-system:inCondition [
               schema:minValue ?min ;
               schema:maxValue ?max ;
           ] ;
       ].
      FILTER (?val < ?min || ?val > ?max)
   }
   """

   target = OM2M_BASE+CSE_NAME
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"smd":find_damaged_sensors})
   print(json.dumps(json.loads(response.content), indent=2))


## Performing offline analysis

Sometimes, data analysis requires a more general context, and relying on online queries is not sufficient.
In such cases, data may be concentrated into a distant repository before being processed.
et us retrieve all the descriptors from the platform to analyze their content.


.. code-block:: python
   :class: exercise

   # First, let's create some observations and their descriptors
   thunder_wind_cin = test_client.create_cin(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/DATA", 87)
   desc=rdflib.Graph()
   desc.parse("ttl/wind_observation_thunder.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/DATA",
                                     base64.b64encode(desc.serialize(format='xml')).decode("utf-8"),
                                     rels=CSE_ID+CSE_NAME+"/Weather_Station/Anemometer/ANEMOMETER_DESCRIPTOR")
   thunder_temperature_cin = test_client.create_cin(CSE_ID+CSE_NAME+"/Weather_Station/Thermometer/DATA", 13)
   desc=rdflib.Graph()
   desc.parse("ttl/temperature_observation_thunder.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Thermometer/DATA",
                                     base64.b64encode(desc.serialize(format='xml')).decode("utf-8"),
                                     rels=CSE_ID+CSE_NAME+"/Weather_Station/Thermometer/THERMOMETER_DESCRIPTOR")
   thunder_pressure_cin = test_client.create_cin(CSE_ID+CSE_NAME+"/Weather_Station/Barometer/DATA", 997)
   desc=rdflib.Graph()
   desc.parse("ttl/pressure_observation_thunder.ttl", format="ttl")
   response = test_client.create_smd(CSE_ID+CSE_NAME+"/Weather_Station/Barometer/DATA",
                                     base64.b64encode(desc.serialize(format='xml')).decode("utf-8"),
                                     rels=CSE_ID+CSE_NAME+"/Weather_Station/Barometer/BAROMETER_DESCRIPTOR")

   # All the data will be accumulated in graph g
   g=rdflib.Graph()
   target = OM2M_BASE+CSE_NAME
   # We want to collect all the data stored in OM2M, so type-based discovery is adapted
   response = requests.get(target, headers={**auth_headers, **common_headers}, params={"fu":1,"ty":24})
   print(json.dumps(json.loads(response.content), indent=2))

   for desc_id in json.loads(response.content)["m2m:uril"]:
       target = OM2M_URL+desc_id
       response = requests.get(target, headers={**auth_headers, **common_headers})
       desc = smd(json_obj=response.content)
       g.parse(data=base64.b64decode(desc.dsp).decode("utf-8"), format="xml")

   # Uncomment the following to visualize the obtained graph
   # print(g.serialize(format="ttl").decode("utf-8"))

   g.parse("ttl/background_knowledge.ttl", format="ttl")

   infer_thunderstorms="""
   PREFIX sosa: <http://www.w3.org/ns/sosa/>
   PREFIX ex: <http://example.org/ns#>
   PREFIX qudt-1-1: <http://qudt.org/1.1/schema/qudt#>
   INSERT {
       ?newObsURI a sosa:Observation;
           sosa:observedProperty ex:adreamThunderStorm .
   } WHERE {
       ?wind_observation a sosa:Observation;
           sosa:observedProperty ex:adreamWindSpeed ;
           sosa:hasResult [
               qudt-1-1:numericValue ?wind_speed;
           ].
       ?temp_observation a sosa:Observation;
           sosa:observedProperty ex:adreamTemperature ;
           sosa:hasResult [
               qudt-1-1:numericValue ?temperature;
           ].
       ?press_observation a sosa:Observation;
           sosa:observedProperty ex:adreamAthmosphericPressure ;
           sosa:hasResult [
               qudt-1-1:numericValue ?pressure;
           ].
       FILTER (?wind_speed>70 && ?temperature<20 && ?pressure<1006)
       BIND(URI(CONCAT("http://example.org/ns#",STRUUID())) AS ?newObsURI)
   }
   """

   g.update(infer_thunderstorms)
   #print(g.serialize(format="ttl").decode("utf-8"))

   infer_inspections="""
   PREFIX sosa: <http://www.w3.org/ns/sosa/>
   PREFIX ex: <http://example.org/ns#>
   PREFIX qudt-1-1: <http://qudt.org/1.1/schema/qudt#>
   PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
   INSERT {
       ?device ex:requiresInspection true .
   } WHERE {
       ?obs a sosa:Observation;
           sosa:observedProperty ex:adreamThunderStorm .
       ?device a/rdfs:subClassOf ex:FragileDevice.

   }
   """

   g.update(infer_inspections)
   print(g.serialize(format="ttl").decode("utf-8"))
