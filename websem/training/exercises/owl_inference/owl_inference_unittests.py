# formation_unittest
"""Check descriptors exercise."""
import unittest
from jupyterlab_training import FormUnitTests
import owlrl


class OwlTest(unittest.TestCase):
    def test_grandMother(self):
        import rdflib
        g = rdflib.Graph()

        g.parse(data=turtle_triple, format="turtle")

        owlrl.DeductiveClosure(owlrl.OWLRL_Semantics).expand(g)
        q = '''
            PREFIX : <http://example.org/relatives#>
            SELECT (COUNT(?gc) AS ?cnt)
            WHERE {
                ?gc :hasGrandparent ?gp .
            }
            '''
        for r in g.query(q):
            cnt = int(r[0])
        self.assertEqual(cnt, 1)


# formation widget
FormUnitTests(OwlTest);
