# formation_solution
turtle_triple = """
@prefix : <http://example.org/relatives#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <http://example.org/relatives> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<http://example.org/relatives> rdf:type owl:Ontology .

#################################################################
#    Object Properties
#################################################################

###  http://example.org/relatives#hasParent
:hasParent rdf:type owl:ObjectProperty ;
    rdfs:domain foaf:Person ;
    rdfs:range foaf:Person .

:hasGrandparent rdf:type owl:ObjectProperty ;
                owl:propertyChainAxiom ( :hasParent
                                         :hasParent
                                       ) .

#################################################################
#    Individuals
#################################################################

:BobSenior rdf:type foaf:Person .

:Bob rdf:type foaf:Person ;
     :hasParent :BobSenior .

:Bill rdf:type :Person ;
      :hasParent :Bob .

"""
