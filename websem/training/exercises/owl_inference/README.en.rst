
Define an ``owl:ObjectProperty`` name ``hasParent`` and another one ``hasGrandparent``,
such that  ``:Bill :hasGrandparent :BobSenior`` is true.

.. code-block:: python
   :class: exercise

   turtle_triple = """
   @base <http://example.org/relatives> .
   @prefix : <http://example.org/relatives#> .
   @prefix owl: <http://www.w3.org/2002/07/owl#> .
   @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
   @prefix xml: <http://www.w3.org/XML/1998/namespace> .
   @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
   @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
   @prefix foaf: <http://xmlns.com/foaf/0.1/> .

   <http://example.org/relatives> rdf:type owl:Ontology .

   #################################################################
   #    Object Properties
   #################################################################

   ## Write your hasParent and hasGrandparent objectProperties here:



   #################################################################
   #    Individuals
   #################################################################

   :BobSenior rdf:type foaf:Person .

   :Bob rdf:type foaf:Person ;
        :hasParent :BobSenior .

   :Bill rdf:type :Person ;
         :hasParent :Bob .

   """

.. literalinclude:: /exos/modules/Web/sem/owl_inference/owl_inference_unittests.py
                    :language: python
                    :class: test

**Solution**

.. literalinclude:: /exos/modules/Web/sem/owl_inference/owl_inference_solutions.py
                    :language: python
                    :class: solution
