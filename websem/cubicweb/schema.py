# -*- coding: utf-8 -*-
# copyright 2019 LOGILAB S.A. (Paris, FRANCE), all rights reserved.
# contact http://www.logilab.fr -- mailto:contact@logilab.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 2.1 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""cubicweb-training schema"""
from yams.buildobjs import (
    Boolean,
    Date,
    EntityType,
    Float,
    RelationDefinition,
    String
)
from yams.constraints import BoundaryConstraint, Attribute
from cubicweb.schema import RRQLExpression

SESSION_TYPES = ['cubicweb', 'python', 'semantic_web']


class Session(EntityType):
    session_type = String(
        required=True, vocabulary=SESSION_TYPES, default=SESSION_TYPES[0]
    )
    location = String(required=True)
    start_date = Date()
    end_date = Date(
        constraints=[
            BoundaryConstraint('>=', Attribute('start_date'))
        ]
    )
    duration = Float(required=True, default=8, description='Hours')
    is_opened = Boolean(
        default=False, description='is the session in progress ?'
    )


class is_trainer(RelationDefinition):
    __permissions__ = {
        "read": ("managers", "users"),
        "add": ("managers", RRQLExpression("O owned_by U")),
        "delete": ("managers", RRQLExpression("O owned_by U")),
    }
    subject = "CWUser"
    object = "Session"
    cardinality = "**"


class participated_in(RelationDefinition):
    __permissions__ = {
        'read': ('managers', 'users'),
        'add': ('managers', RRQLExpression('O owned_by U')),
        'delete': ('managers', RRQLExpression('O owned_by U')),
    }
    subject = 'CWUser'
    object = ('Session')
    cardinality = '**'


class Organisation(EntityType):
    name = String(required=True)
    location = String(required=True)


class employ(RelationDefinition):
    subject = 'Organisation'
    object = 'CWUser'
    cardinality = "**"
