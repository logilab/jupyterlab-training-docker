# pylint: disable-msg=W0622
"""cubicweb-file packaging information"""

modname = 'cubicweb_training'
distname = "cubicweb-training"

numversion = (0, 1, 0)
version = '.'.join(str(num) for num in numversion)

license = 'LGPL'
author = "Logilab"
author_email = "contact@logilab.fr"
web = 'https://www.cubicweb.org/project/%s' % distname
description = "file component for the CubicWeb framework"
classifiers = [
           'Environment :: Web Environment',
           'Framework :: CubicWeb',
           'Programming Language :: Python',
           'Programming Language :: JavaScript',
]
__depends__ = {'six': '>= 1.4.0',
               'cubicweb': '>= 3.26.14',
               'cubicweb-rqlcontroller': '>=0.5',
               'cubicweb-signedrequest': '<0.5.1'}

__recommends__ = {}
