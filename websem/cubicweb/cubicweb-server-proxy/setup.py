import setuptools

setuptools.setup(
    name="jupyter-cubicweb-proxy",
    version='1.0',
    author="Logilab",
    description="ogiorgis@logilab.fr",
    packages=setuptools.find_packages(),
	keywords=['Jupyter', 'cubicweb'],
    install_requires=[
        'jupyter-server-proxy'
    ],
    entry_points={
        'jupyter_serverproxy_servers': [
            'cubicweb = cubicweb_server_proxy:setup_cubicweb',
        ]
    },
    package_data={
        'cubicweb_server_proxy': ['icons/*'],
    },
)
