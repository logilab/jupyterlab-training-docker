"""
Return config on servers to start for cubicweb
"""
import os
import shutil

def setup_cubicweb():
    return {
        'command': ['/opt/conda/bin/cubicweb-ctl', 'pyramid', '-Dlinfo', 'training'],
        'absolute_url': False,
        'port': 8080,
        'timeout': 40,
        'launcher_entry': {
            'enabled': True,
            'icon_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'icons', 'cubicweb.svg'),
            'title': "cubicweb",
        }
    }
