import './rql';

export default [{
    id: 'jupyterlab-rql-highlight',
    autoStart: true,
    activate: function(app) {
      console.log('JupyterLab extension jupyterlab-rql-highlight is activated!');
      console.log(app.commands);
      registerRqlFileType(app);
    }
}];

function registerRqlFileType(app) {
  app.docRegistry.addFileType({
    name: 'rql',
    displayName: 'Rql',
    extensions: ['rql'],
    mimeTypes: ['text/x-rql'],
  });
}
