// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE
import * as CodeMirror from 'codemirror';
import 'codemirror/addon/mode/simple';

// "use strict";

CodeMirror.defineSimpleMode("rql",{
  // The start state contains the rules that are intially used
  start: [
    // Strings
    { regex: /"(?:[^\\]|\\.)*?"/, token: "string" },
    { regex: /'(?:[^\\]|\\.)*?'/, token: "string" },

    // Decimal Numbers
    {regex: /\b[+-]?(?:[0-9]+(?:\.[0-9]+)?|\.[0-9]+|\.)(?:[eE][+-]?[0-9]+)?[i]?\b/,
     token: 'number'},

    // keywords
    {regex: /(?:Any|is|AND|ASC|BEING|DELETE|DESC|DISTINCT|EXISTS|FALSE|GROUPBY)\b/,token: "atom"},
    {regex: /(?:HAVING|ILIKE|INSERT|LIKE|LIMIT|NOT|NOW|NULL|OFFSET|OR|ORDERBY)\b/,token: "keyword"},
    {regex: /(?:REGEXP|SET|TODAY|TRUE|UNION|WHERE|WITH)\b/,token: "keyword"},

    // operators
    {regex: /-|=|<=|>=|<<|>>|<|>|%|&|!=/, token: 'operator'},
    {regex: /\*|\+|\^|\/|!|~|=|~=/, token: 'operator'},

    // comment
    {regex: /#.*/, token: "comment"},
  ],
  meta: {
    closeBrackets: {pairs: "()[]{}`'\"\""},
    dontIndentStates: ['comment'],
    lineComment: '#',
  }
});

CodeMirror.defineMIME('text/x-rql', 'rql');
CodeMirror.defineMIME('text/rql', 'rql');

// When I paste this file in Jupyter, it won't work unless I include the
// following code, but when I leave this as a separate module, it won't work and
// raises an error.
CodeMirror.modeInfo.push({
  ext: ['rql'],
  mime: "text/x-rql",
  mode: 'rql',
  name: 'Rql'
});
