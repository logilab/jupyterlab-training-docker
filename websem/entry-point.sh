#!/usr/bin/env bash

# workspace persistence
if [ ! -d /home/jovyan/training/exercises ]; then
    cp -r /home/jovyan/training-websem-init/* /home/jovyan/training
fi

# jupyterlab global state persistence
if [ ! -d /home/jovyan/training/.jupyter ]; then
    cp -r /home/jovyan/.jupyter /home/jovyan/training/.jupyter
fi
rm -rf /home/jovyan/.jupyter
cd /home/jovyan; ln -s /home/jovyan/training/.jupyter

# jupyterhub start
tini -g -- start-singleuser.sh "$@"
